<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;
        \DB::table('categories')->truncate();
        \DB::table('categories')->insert([
            [
                'name'  =>  json_encode([
                    'ar'    =>  'فواكة',
                    'en'    =>  'Mobiles'
                ])
            ],
            [
                'name'  =>  json_encode([
                    'ar'    =>  'خضروات',
                    'en'    =>  'Computer'
                ])
            ]
        ]);


        \DB::table('products')->truncate();
        $i = 1;
        \DB::table('products')->insert([
            [
                'category_id'   =>  1,
                'name'  =>  json_encode([
                    'ar'    =>  'موز',
                    'en'    =>  'Product name' . $i++
                ]),
                'brief' =>  json_encode([
                    'ar'    =>  'مووووووووووووز',
                    'en'    =>  'Product description Product description Product description Product description Product description '
                ]),
                'price' =>  rand(1000, 10000),
                'stock' =>  10,
                'quantity'  =>  1
            ],
            [
                'category_id'   =>  1,
                'name'  =>  json_encode([
                    'ar'    =>  'فراولة',
                    'en'    =>  'Product name' . $i++
                ]),
                'brief' =>  json_encode([
                    'ar'    =>  'فراووووووووووووولة',
                    'en'    =>  'Product description Product description Product description Product description Product description '
                ]),
                'price' =>  rand(1000, 10000),
                'stock' =>  10,
                'quantity'  =>  1
            ],
        ]);
    }
}
