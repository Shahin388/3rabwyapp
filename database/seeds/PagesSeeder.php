<?php

use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('pages')->truncate();

        \DB::table('pages')->insert([
            [
                'type'  =>  'terms',
                'title' =>  json_encode([
                    'ar'    =>  'الشروط والأحكام',
                    'en'    =>  'Terms & Conditions'
                ]),
                'content'   =>  json_encode([
                    'ar'    =>  'محتوى صفحة الشروط والأحكام محتوى صفحة الشروط والأحكام محتوى صفحة الشروط والأحكام محتوى صفحة الشروط والأحكام ',
                    'en'    =>  'Content of terms and conditions page Content of terms and conditions page Content of terms and conditions page Content of terms and conditions page'
                ])
            ],
            [
                'type'  =>  'policy',
                'title' =>  json_encode([
                    'ar'    =>  'سياسة الخصوصية',
                    'en'    =>  'Privacy & policy'
                ]),
                'content'   =>  json_encode([
                    'ar'    =>  'محتوى صفحة سياسة الخصوصية محتوى صفحة سياسة الخصوصية محتوى صفحة سياسة الخصوصية محتوى صفحة سياسة الخصوصية ',
                    'en'    =>  'Content of privacy and policy page Content of privacy and policy page Content of privacy and policy page Content of privacy and policy page'
                ])
            ],
            [
                'type'  =>  'return_policy',
                'title' =>  json_encode([
                    'ar'    =>  'سياسة الاسترجاع',
                    'en'    =>  'Privacy & policy'
                ]),
                'content'   =>  json_encode([
                    'ar'    =>  'محتوى صفحة سياسة الاسترجاع محتوى صفحة سياسة الاسترجاع محتوى صفحة سياسة الاسترجاع محتوى صفحة سياسة الاسترجاع ',
                    'en'    =>  'Content of return policy page Content of return policy page Content of return policy page Content of return policy page'
                ])
            ],
            [
                'type'  =>  'about',
                'title' =>  json_encode([
                    'ar'    =>  'من نحن',
                    'en'    =>  'About us'
                ]),
                'content'   =>  json_encode([
                    'ar'    =>  'محتوى صفحة من نحن محتوى صفحة من نحن محتوى صفحة من نحن محتوى صفحة من نحن محتوى صفحة من نحن ',
                    'en'    =>  'Content of about us page Content of about us page Content of about us page Content of about us page '
                ])
            ],
            [
                'type'  =>  'page',
                'title' =>  json_encode([
                    'ar'    =>  'سياسية الخصوصية',
                    'en'    =>  'Privacy policy'
                ]),
                'content'   =>  json_encode([
                    'ar'    =>  'نص مثال للمحتوى المتغير للصفحة نص مثال للمحتوى المتغير للصفحة نص مثال للمحتوى المتغير للصفحة ',
                    'en'    =>  'Changable text content Changable text content Changable text content Changable text content '
                ])
            ],
            [
                'type'  =>  'page',
                'title' =>  json_encode([
                    'ar'    =>  'سياسة الاسترجاع',
                    'en'    =>  'Return policy'
                ]),
                'content'   =>  json_encode([
                    'ar'    =>  'نص مثال للمحتوى المتغير للصفحة نص مثال للمحتوى المتغير للصفحة نص مثال للمحتوى المتغير للصفحة ',
                    'en'    =>  'Changable text content Changable text content Changable text content Changable text content '
                ])
            ]
        ]);
    }
}
