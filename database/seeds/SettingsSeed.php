<?php

use Illuminate\Database\Seeder;

class SettingsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('app_settings')->truncate();

        \DB::table('app_settings')->insert([
            [
                'key'   =>  'address',
                'value' =>  json_encode([
                    'ar'    =>  'الكويت - مدينة الكويت',
                    'en'    =>  'Kuwit - Kuwit city'
                ])
            ],
            [
                'key'   =>  'phone',
                'value' =>  json_encode([
                    'ar'    =>  "+0154488748787",
                    'en'    =>  "+01458454748"
                ])
            ],
            [
                'key'   =>  'email',
                'value' =>  json_encode([
                    'ar'    =>  'app@test.com',
                    'en'    =>  'app@test.com'
                ])
            ]
        ]);
    }
}
