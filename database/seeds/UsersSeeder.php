<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->truncate();
        \DB::table('users')->insert([
            [
                'username'    =>  'super admin',
                'email'         =>  'admin@admin.com',
                'password'  =>  bcrypt('123123'),
                'mobile'   =>   '02132323121321'
            ]
        ]);
    }
}
