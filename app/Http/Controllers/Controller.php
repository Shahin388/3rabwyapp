<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $user = auth('api')->user();
        $token = request()->header('FbToken');
        if ($user && $token) {
            $platform = request()->header('Platform') ?? 'android';
            $user->device()->firstOrCreate(['device_token' => $token, 'device_type' => $platform]);
        }
    }
}
