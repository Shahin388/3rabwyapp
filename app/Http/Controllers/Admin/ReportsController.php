<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Orders\Models\Order;
use Modules\User\Models\User;
use Modules\Areas\Models\City;
use Modules\Areas\Models\Country;
use Modules\Areas\Models\State;

class ReportsController extends Controller
{
    public function ordersReport()
    {
        $orders = Order::latest()->get();
        $totalPrice = Order::sum('package_price');
        //dd($totalPrice);
        // $clients = User::where('type', 'client')->count();
        // $agents = User::where('type', 'agent')->count();
        // $cities = City::count();
        // $countries = Country::count();
        // $states = State::count();
        // //dd($clients);
        $deliveryPrice = Order::sum('delivery_price');

        return view('admin.reports.orderReport', compact('orders', 'totalPrice', 'deliveryPrice'));
    }


    public function reports()
    {
        $orders = Order::latest()->get();
        $totalPrice = Order::sum('package_price');
        //dd($totalPrice);
        $clients = User::where('type', 'client')->count();
        $agents = User::where('type', 'agent')->count();
        $cities = City::count();
        $countries = Country::count();
        $states = State::count();
        //dd($clients);
        $deliveryPrice = Order::sum('delivery_price');

        return view('admin.reports.index', compact('orders', 'totalPrice', 'deliveryPrice', 'clients', 'agents', 'cities', 'countries', 'states'));
    }
}
