<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\User\Models\User;
use App\Models\Rate;
use App\Models\Trip;
use App\Models\TripDetails;
use App\Models\Notification;
use Modules\Categories\Models\Place;
use Modules\Categories\Models\PlaceAgent;

use Carbon;
use Auth;
use Validator;
use File;
use Hash;
use DB;

class HomeController extends Controller
{
    public function addRate()
    {
        $validator = Validator::make(request()->all(), [
      'user_id' => 'required',
      'value' => 'required',
      'comment' => 'required'
      ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()], 400);
        }

        if (Auth::user()->id == request('user_id')) {
            return response()->json(['status' => 0, 'message' => 'You can not rate yourself.'], 400);
        }

        $rateId = Rate::where('rateable_id', Auth::id())->where('user_id', request('user_id'))->pluck('id', 'user_id')->first();
        if (empty($rateId)) {
            $rate = new Rate([
        'rateable_id' => Auth::id(),
        'user_id' => request('user_id'),
        'value' => request('value'),
        'comment' => request('comment')
        ]);
            $rate->save();


            $user = User::find($rate->user_id);

            // $notification = Notification::create([
            //     'user_id' => $user->id,
            //     'content' => 'قام '. Auth::user()->name . 'بتقييمك ',
            //     'from_user_id' => Auth::id(),
            //     'type' => 'rate',
            //     'notify_type' => 'rate'
            //     ]);
            // if (!empty($user->token)) {
            //     //dd($user->token);
            //     NotificationsRepository::pushNotification($user->token, 'تقييم جديد', $notification->content, ['user_id' => $user->id, 'type' =>'rate', 'status' => 'قام'. Auth::user()->name . ' بتقييمك  ']);
            // }

            return response()->json(['status' => 1, 'message' => 'User was successfully rated!'], 200);
        } else {
            $rate = Rate::find($rateId);

            $user = User::find($rate->user_id);

            // $notification = Notification::create([
            //     'user_id' => $user->id,
            //     'content' => 'قام '. Auth::user()->name . 'بتقييمك ',
            //     'from_user_id' => Auth::id(),
            //     'type' => 'rate',
            //     'notify_type' => 'rate'
            //     ]);
            // if (!empty($user->token)) {
            //     //dd($user->token);
            //     NotificationsRepository::pushNotification($user->token, 'تقييم جديد', $notification->content, ['user_id' => $user->id, 'type' =>'rate', 'status' => 'قام'. Auth::user()->name . ' بتقييمك  ']);
            // }

            $rate->update(['rateable_id' => Auth::id(),'user_id' => request('user_id'),'value' => request('value'),'comment' => request('comment')]);

            return response()->json(['status' => 1, 'message' => 'User was successfully update rated!'], 200);
        }
    }


    public function currentTrips()
    {
        $trips = collect(Trip::where('status', 1)->latest()->get()->each(function ($trip) {
            $trip->ago = empty($trip->date) ? '' : \Carbon\Carbon::parse($trip->date)->diffForHumans();
        }));



        return response()->json([
            'status' => 1,
            'data' => $trips
            ], 200);
    }
    
     public function logout()
    {
        Auth::logout();
        return response()->json(['status' => 1, 'message' => 'User Logout successfully'], 200);
    }

    public function finishedTrips()
    {
        $trips = collect(Trip::where('status', 3)->latest()->get()->each(function ($trip) {
            $trip->ago = empty($trip->date) ? '' : \Carbon\Carbon::parse($trip->date)->diffForHumans();
        }));



        return response()->json([
            'status' => 1,
            'data' => $trips
            ], 200);
    }


    public function acceptTrip()
    {
        $validator = Validator::make(request()->all(), ['trip_id' => 'required']);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()], 400);
        }
        $trip = Trip::find(request('trip_id'));

        if (empty($trip)) {
            return response()->json(['status' => 1, 'message' => 'trip no found'], 200);
        }

        $trip = Trip::find(request('trip_id'));
        $trip->status = 1;
        $trip->save();


        $trip = collect($trip)->except(['created_at', 'updated_at']);

        return response()->json(['status' => 1, 'data' => $trip], 200);
    }
    
    public function userPlace()
    {
        $validator = Validator::make(request()->all(), ['place_id' => 'required']);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()], 400);
        }
        $placeID = Place::where('place_id',request('place_id'))->pluck('id')->first();
        

        if (empty($placeID)) {
            return response()->json(['status' => 1, 'message' => 'place no found'], 200);
        }

        $userPlace = PlaceAgent::where('agent_id',Auth::id())->where('place_id',$placeID)->pluck('id')->first();
        $count = PlaceAgent::where('place_id',$placeID)->count();
       // dd($count);
        
        if($userPlace){
                    return response()->json(['status' => 1, 'data' => ['isTaken' => 1,'count' =>$count]], 200);

        }else{
                   return response()->json(['status' => 1, 'data' => ['isTaken' => 0,'count' =>$count]], 200);

        }
    }

    public function refuseTrip()
    {
        $validator = Validator::make(request()->all(), ['trip_id' => 'required']);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()], 400);
        }
        $trip = Trip::find(request('trip_id'));

        if (empty($trip)) {
            return response()->json(['status' => 1, 'message' => 'trip no found'], 200);
        }

        $trip = Trip::find(request('trip_id'));
        $trip->status = 2;
        $trip->save();


        $trip = collect($trip)->except(['created_at', 'updated_at']);

        return response()->json(['status' => 1, 'data' => $trip], 200);
    }


    public function finishTrip()
    {
        $validator = Validator::make(request()->all(), ['trip_id' => 'required']);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()], 400);
        }
        $trip = Trip::find(request('trip_id'));

        if (empty($trip)) {
            return response()->json(['status' => 1, 'message' => 'trip no found'], 200);
        }

        $trip = Trip::find(request('trip_id'));
        $trip->status = 3;
        $trip->save();


        $trip = collect($trip)->except(['created_at', 'updated_at']);

        return response()->json(['status' => 1, 'data' => $trip], 200);
    }


    public function addTripDetails()
    {
        $validator = Validator::make(request()->all(), [
        'tripd_id' => 'required',
        'received_name' => 'required',
        'received_phone' => 'required',
        'delivery_charge' => 'required',
        'details' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()], 400);
        }

        $trip = Trip::find(request('tripd_id'));
        if (empty($trip)) {
            return response()->json(['status' => 0, 'message' => 'trip not found'], 200);
        }

        $data = [
        'tripd_id' => request('tripd_id'),
        'received_name' => request('received_name'),
        'received_phone' => request('received_phone'),
        'delivery_charge' => request('delivery_charge'),
        'details' => request('details'),
        'user_id' => Auth::id()
        ];
        $tripDetails = new TripDetails($data);
        $tripDetails->save();

        return response()->json(['status' => 1, 'data' => $tripDetails], 200);
    }


    public function getNotifications()
    {
        $notifications = Notification::where('user_id', Auth::id())->latest()->get();



        if (count($notifications) < 1) {
            return response()->json(['status' => 1, 'data' => []], 200);
        }

        foreach ($notifications as $notification) {
            $notification->update(['is_read' => 1]);
            $notification->type = empty($notification->type) ? '' : $notification->type;
            $notification->content = empty($notification->content) ? '' : $notification->content;
            $notification->from_user_name = empty($notification->fromUser)? '' :$notification->fromUser->username ;
            $notification->from_user_avatar = empty($notification->fromUser->image) ? '' : $notification->fromUser->image;
            $notification->time = empty($notification->created_at) ? '' : $notification->created_at->format('H:i A');
            $notification->date = empty($notification->created_at) ? '' : $notification->created_at->format('d M');
        }



        $notifications = $notifications->map(function ($notification) {
            return collect($notification)->except(['from_user']);
        });

        return response()->json(['status' => 1, 'data' => $notifications], 200);
    }
}
