<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\User\Models\User;
use App\Models\ChatRoom;
use App\Models\ChatMessage;

use Auth;
use Validator;

class ChatController extends Controller
{
    public function chatRoom()
    {
        $validator = Validator::make(request()->all(), ['receiver_id' => 'required']);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()], 400);
        }
        $user = Auth::id();
        $receiver = request('receiver_id');


        if ($user == $receiver) {
            return response()->json([
                        'status' => 0,
                        'message' => 'error you can\'t chat with yourself'
                        ], 409);
        }

        $chat = ChatRoom::whereIn('to_user_id', [$user, $receiver])->whereIn('from_user_id', [$user, $receiver])->first();
        if (isset($chat)) {
            $messages = ChatMessage::where('chat_room_id', $chat->id)->latest()->get()->each(function ($message) {
                $message->update(['is_read' => 1]);
                $message->to_user_image = empty($message->toUser->image) ? '' : $message->toUser->image;
                $message->from_user_image = empty($message->fromUser->image) ? '' : $message->fromUser->image;
                $message->to_user_char = empty($message->toUser->name) ? '' : substr($message->toUser->name, 0, 1);
                $message->from_user_char = empty($message->fromUser->name) ? '' : substr($message->fromUser->name, 0, 1);
                $message->time = date('h:i A', strtotime($message->created_at));
            })->map(function ($message) {
                return collect($message)->except(['to_user','from_user', 'updated_at']);
            });

            return response()->json(['status'=> 1,'chat_room' => $chat,'messages' => $messages], 200);
        }

        $new_chat = ChatRoom::create([
            'from_user_id' => $user,
            'to_user_id' => $receiver
            ]);

        return response()->json([ 'chat_room' => $new_chat, 'messages' => []], 200);
    }

    public function getRooms()
    {
        // return response()->json(['me' => Auth::user()]);
        $rooms = ChatRoom::where('from_user_id', Auth::id())->orWhere('to_user_id', Auth::id())->get();
        foreach ($rooms as $room) {
            if ($room->from_user_id == Auth::id()) {
                $room->from_user_id = $room->to_user_id;
                $room->to_user_id = Auth::id();
            }
            // $room->message = $room->chatMessages->orderBy('id', 'desc')->first()->message;
            $room->message = ChatMessage::where('chat_room_id', $room->id)->latest()->first() ? ChatMessage::where('chat_room_id', $room->id)->latest()->first()->message : '' ;
            $room->date = date('d/m/y', strtotime($room->created_at));
            $room->from_user_name = $room->fromUser->name;
            $room->from_user_char = substr($room->fromUser->name, 0, 1);
            $room->count_messages = count($room->chatMessages);
            $room->image = empty($room->fromUser->image) ? '' : $room->fromUser->image;
        }

        $rooms = $rooms->map(function ($room) {
            return collect($room)->only(['id', 'message', 'img','from_user_id', 'from_user_name', 'from_user_char','count_messages','date', 'image']);
        });

        return response()->json(['status' => 1, 'rooms' => $rooms], 200);
    }

    public function addMessage()
    {
        $validator = Validator::make(request()->all(), [
            'message' => 'nullable',
            'chat_room_id' => 'required',
            'to_user_id' => 'required'
            ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()], 400);
        }
        $chat_room_id = request('chat_room_id');
        $chatRoom = ChatRoom::whereIn('from_user_id', [Auth::id(), request('to_user_id')])->whereIn('to_user_id', [Auth::id(), request('to_user_id')])->first();
        if (!isset($chatRoom)) {
            $new_chat = ChatRoom::create([
            'from_user_id' => Auth::id(),
            'to_user_id' => request('to_user_id')
            ]);
            $chat_room_id = $new_chat->id;
            // return response()->json(['messages' => 'Failed', 'data' => 'There is no Chat Room between yours'], 400);
        }

        $message = new ChatMessage([
            'message' => request('message') ? request('message'): "",
            'img' => request('img') ? uploadImgFromMobile(request('img'), 'user') : "",
            'chat_room_id' => $chat_room_id,
            'to_user_id' => request('to_user_id'),
            'from_user_id' => Auth::id()
            ]);
        if ($message->save()) {
            //$user = User::find(request('to_user_id'));

            // $notification = Notification::create([
            //   'user_id' => $user->id,
            //   'content' => 'رسالة جديدة',
            //   'from_user_id' => Auth::id(),
            //   'type' => 'message',
            //   'notify_type' => 'message'
            //   ]);
            //
            // if (!empty($user->token)) {
            //     NotificationsRepository::pushNotification($user->token, 'رسالة جديدة', $notification->fromUser->username . $notification->content, ['user_id' => $user->id, 'type' =>'message', 'message' => 'يوجد رسالة جديدة']);
            // }

            // NotificationsRepository::pushNotification();
            return response()->json(['status' => 1, 'data' => $message,'message'=>'تم ارسال الرسالة بنجاح'], 200);
        }

        return response()->json(['status' => 0, 'message' => 'Something Wrong'], 409);
    }

    public function deleteChat()
    {
        $validator = Validator::make(request()->all(), [
            'chat_room_id' => 'required',
            ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()], 200);
        }
        $chatroom = ChatRoom::find(request('chat_room_id'));
        $chatmessages = ChatMessage::where('chat_room_id', request('chat_room_id'))->where('from_user_id', Auth::id())->get();

        if (empty($chatroom)) {
            return response()->json(['status' => 0, 'message' => 'there is no chat'], 200);
        }
        if (!empty($chatmessages)) {
            $chatmessages = ChatMessage::where('chat_room_id', request('chat_room_id'))->where('from_user_id', Auth::id())->delete();
        }
        $chatroom->delete();




        return response()->json(['status' => 1, 'message' => 'Deleted'], 409);
    }
}
