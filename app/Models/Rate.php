<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;

class Rate extends Model
{
    protected $fillable = [
        'value', 'user_id', 'rateable_id', 'comment', 'rate', 'rated_id', 'rated_type'
    ];



    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function fromUser()
    {
        return $this->belongsTo(User::class, 'rateable_id');
    }
}
