<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;

class ChatMessage extends Model
{
    protected $fillable = [
        'from_user_id', 'to_user_id', 'chat_room_id', 'message', 'is_read',
    ];

    public function toUser()
    {
        return $this->belongsTo(User::class, 'to_user_id');
    }

    public function fromUser()
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }

    public function chatRoom()
    {
        return $this->belongsTo('App\Models\ChatRoom', 'chat_room_id');
    }
}
