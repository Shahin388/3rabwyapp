<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;

class ChatRoom extends Model
{
    protected $fillable = [
        'from_user_id', 'to_user_id', 'type',
    ];

    public function toUser()
    {
        return $this->belongsTo(User::class, 'to_user_id');
    }

    public function fromUser()
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }



    public function chatMessages()
    {
        return $this->hasMany('App\Models\ChatMessage');
    }
}
