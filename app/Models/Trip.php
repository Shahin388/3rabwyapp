<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;
use Modules\CarTypes\Models\Type;

class Trip extends Model
{
    protected $fillable = [
  'user_id', 'driver_id','car_id','date','start_time','end_time','status','from','from_lat','from_lang','to','to_lat','to_lang','price',
];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function driver()
    {
        return $this->belongsTo(User::class, 'driver_id');
    }

    public function car()
    {
        return $this->belongsTo(Type::class, 'car_id');
    }
}
