<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TripDetails extends Model
{
    protected $fillable = [
'trip_id', 'user_id','received_name','received_phone','delivery_charge','details',
];

    public function trip()
    {
        return $this->belongsTo('App\Models\Trip', 'user_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
