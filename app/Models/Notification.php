<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;
use Modules\Orders\Models\Order;

class Notification extends Model
{
    protected $fillable = [
'content', 'is_read', 'user_id','from_user_id','order_id','type','notify_type','is_active'
];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function fromUser()
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
