@extends('Common::admin.layout.page')
@section('page')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Total Orders Price</h3>
                <a href="#" class="mlink btn btn-success">
                    <span>{{ $totalPrice }}</span></a>
            </div>

            <div class="card-header">
                <h3 class="card-title">Total Delivery Price</h3>
                <a href="#" class="mlink btn btn-success">
                    <span>{{ $deliveryPrice }}</span></a>
            </div>

            
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Price</th>
                            <th>@lang('Client')</th>
                            <th>@lang('Barber')</th>
                            <th>@lang('Created at')</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->package_price ?? 0 }}</td>
                            <td>{{ $order->user->username ?? $order->guest->name ?? '#' }}</td>
                            <td>{{ $order->agent->username ?? '#' }}</td>
                            <th>{{ $order->created_at }}</th>

                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<script>
    // $('.table').DataTable({
    //     "paging": false,
    //     "lengthChange": false,
    //     "searching": false,
    //     "ordering": true,
    //     "info": false,
    //     "autoWidth": false,
    // });

</script>

@stop
