@extends('Common::admin.layout.page')
@section('page')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Total Orders Price</h3>
                <a href="{{url('admin/ordersReport')}}" class="mlink btn btn-success">
                    <span>{{ $totalPrice }}</span></a>
            </div>

            <div class="card-header">
                <h3 class="card-title">Total Delivery Price</h3>
                <a href="#" class="mlink btn btn-success">
                    <span>{{ $deliveryPrice }}</span></a>
            </div>

            <div class="card-header">
                <h3 class="card-title"> Num Of Clients</h3>
                <a href="#" class="mlink btn btn-success">
                    <span>{{ $clients }}</span></a>
            </div>

            <div class="card-header">
                <h3 class="card-title"> Num Of Agents</h3>
                <a href="#" class="mlink btn btn-success">
                    <span>{{ $agents }}</span></a>
            </div>

            <div class="card-header">
                <h3 class="card-title"> Num Of Countries</h3>
                <a href="#" class="mlink btn btn-success">
                    <span>{{ $countries }}</span></a>
            </div>

            <div class="card-header">
                <h3 class="card-title"> Num Of Cities</h3>
                <a href="#" class="mlink btn btn-success">
                    <span>{{ $cities }}</span></a>
            </div>

            <div class="card-header">
                <h3 class="card-title"> Num Of States</h3>
                <a href="#" class="mlink btn btn-success">
                    <span>{{ $states }}</span></a>
            </div>
            <!-- /.card-header -->
            <!-- <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Price</th>
                            <th>@lang('Client')</th>
                            <th>@lang('Barber')</th>
                            <th>@lang('Created at')</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->package_price ?? 0 }}</td>
                            <td>{{ $order->user->username ?? $order->guest->name ?? '#' }}</td>
                            <td>{{ $order->agent->username ?? '#' }}</td>
                            <th>{{ $order->created_at }}</th>

                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div> -->
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<script>
    // $('.table').DataTable({
    //     "paging": false,
    //     "lengthChange": false,
    //     "searching": false,
    //     "ordering": true,
    //     "info": false,
    //     "autoWidth": false,
    // });

</script>

@stop
