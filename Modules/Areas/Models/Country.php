<?php

namespace Modules\Areas\Models;

use Modules\Common\Models\HelperModel;

class Country extends HelperModel
{
    protected $table = 'countries';
    protected $fillable = ['name', 'dial_code'];

    protected $casts = ['name' => 'array'];
    protected $hidden = ['created_at', 'updated_at'];

    public function states()
    {
        return $this->hasMany(State::class, 'country_id')->with('cities');
    }
}
