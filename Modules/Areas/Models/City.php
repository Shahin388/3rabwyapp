<?php

namespace Modules\Areas\Models;

use Illuminate\Database\Eloquent\Builder;

class City extends Area
{   
    protected static function booted()
    {
        static::addGlobalScope('iscity', function (Builder $builder) {
            $builder->where('state_id', '!=', null);
        });
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }
}
