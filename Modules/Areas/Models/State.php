<?php

namespace Modules\Areas\Models;

use Illuminate\Database\Eloquent\Builder;

class State extends Area
{
    
    protected static function booted()
    {
        static::addGlobalScope('isstate', function (Builder $builder) {
            $builder->where('state_id', null);
        });
    }

    public function cities()
    {
        return $this->hasMany(City::class, 'state_id')->select('id' , 'state_id' , 'name');
    }
}
