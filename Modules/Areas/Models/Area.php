<?php

namespace Modules\Areas\Models;

use Modules\Common\Models\HelperModel;

class Area extends HelperModel
{
    protected $table = 'areas';
    protected $fillable = ['name', 'state_id'];

    protected $casts = ['name' => 'array'];
}
