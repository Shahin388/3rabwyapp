<?php
Route::get('states', 'ApiController@states');
Route::get('countries', 'ApiController@countries');
Route::post('countryStates', 'ApiController@countryStates');
Route::post('stateAreas', 'ApiController@stateAreas');
