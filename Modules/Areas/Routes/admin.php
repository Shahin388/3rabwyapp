<?php
Route::resource('countries', 'CountryController');
Route::resource('cities', 'CityController');
Route::resource('states', 'StateController');
