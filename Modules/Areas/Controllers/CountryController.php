<?php

namespace Modules\Areas\Controllers;

use Modules\Areas\Models\Country;
use Modules\Common\Controllers\HelperController;

class CountryController extends HelperController
{
    public function __construct()
    {
        $this->model = new Country();
        $this->title = __('Countries');
        $this->name =  'countries';
        $this->list = ['name' => 'الاسم', 'dial_code' => 'مفتاج الدولة'];

        $this->lang_inputs = [
            'name' => ['title' =>  'الاسم ']
        ];
        $this->inputs = [
            'dial_code' => ['title' => 'مفتاح الدولة']
        ];
    }
}
