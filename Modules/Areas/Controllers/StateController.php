<?php

namespace Modules\Areas\Controllers;

use Modules\Areas\Models\State;
use Modules\Common\Controllers\HelperController;

class StateController extends HelperController
{
    public function __construct()
    {
        $this->model = new State;
        $this->title = __('States');
        $this->name =  'states';
        $this->list = ['name' => 'الاسم'];

        $this->lang_inputs = [
            'name' => ['title' =>  'الاسم ']
        ];
        
    }
}
