<?php

namespace Modules\Areas\Controllers;

use App\Http\Controllers\Controller;
use Modules\Areas\Models\Country;
use Modules\Areas\Models\State;
use Modules\Areas\Models\Area;

class ApiController extends Controller
{

    public function states()
    {
        return api_response(1, '', State::with('cities')->get(['id', 'name']));
    }

    public function countries()
    {
        return api_response(1, '', Country::with('states')->get());
    }
    
    public function countryStates()
    {
      $states = State::when(request('country_id'), function ($query) {
          return $query->where('country_id', request('country_id'));
      })->get();
      return api_response(1, '', $states);
    }
    
    public function stateAreas()
    {
      $areas = Area::when(request('state_id'), function ($query) {
          return $query->where('state_id', request('state_id'));
      })->get();
      return api_response(1, '', $areas);
    }
}
