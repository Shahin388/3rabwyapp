<?php

$menu['Content']['Areas'] = [
    'title' =>  __('Areas'),
    'link'  =>  '#',
    'icon'  =>  'fas fa-map-marker',
    'childs'    =>  [
        [
            'title' =>  __("Countries"),
            'link'  =>  'countries.index'
        ],
        [
            'title' =>  __("States"),
            'link'  =>  'states.index'
        ],
        [
            'title' =>  __('Cities'),
            'link'  =>  'cities.index'
        ]
    ]

];
