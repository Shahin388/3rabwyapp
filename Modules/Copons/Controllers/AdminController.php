<?php

namespace Modules\Copons\Controllers;

use Modules\Common\Controllers\HelperController;
use Modules\Copons\Models\Copon;

class AdminController extends HelperController
{
    public function __construct()
    {
        $this->model = new Copon();
        $this->title = 'Copons';
        $this->name =  'copons';
        $this->list = ['code' => 'الكود', 'using' => 'مرات الاستخدام', 'ended_at' => 'تاريخ الانتهاء'];

        $values = ['usual' => 'قيمة', 'precentage' => 'نسبة مئوية'];
        $this->inputs = [
            'code'  =>  ['title' => 'كود الكوبون', 'type' => 'number'],
            'type'  =>  ['title' => 'النوع', 'type' => 'select', 'values' => $values],
            'discount'  =>  ['title' => 'الخصم', 'type' => 'number'],
            'max_discount'  =>  ['title' => 'أقصى قيمة خصم', 'type' => 'number', 'empty' => 1],
            'ended_at'  =>  ['title' => 'تاريخ الانتهاء', 'type' => 'date']

        ];
    }
}
