<?php

namespace Modules\Copons\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Copons\Models\Copon;

class ApiController extends Controller
{

    public function check_copon(Request $request)
    {
        $this->validate($request, [
            'code'  =>  'required',
            'total' =>  'required|numeric'
        ]);
        $total = request('total');
        $copon = Copon::valid()->whereCode(request('code'))->first();
        if(!$copon){
            return api_response('error' , __("Copon code is invalid"));
        }
        $discount = $copon->discount;
        if ($copon->type == 'precentage') {
            $discount = $total * ($copon->discount / 100);
            $discount = $copon->max_discount && $discount > $copon->max_discount ? $copon->max_discount : $discount;
        }
        return api_response(1, '', ['copon_id' => $copon->id, 'discount' => round($discount, 1), 'total' => round(($total - $discount), 1)]);
    }
}
