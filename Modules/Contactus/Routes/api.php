<?php

Route::post('contactus', 'ApiController@send_message');
Route::post('request', 'ApiController@send_request');
Route::get('contacts', 'ApiController@contacts');
Route::get('socials', 'ApiController@socials');
