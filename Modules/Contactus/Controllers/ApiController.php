<?php

namespace Modules\Contactus\Controllers;

use App\Http\Controllers\Controller;
use Modules\Common\Models\Setting;
use Modules\Contactus\Models\Contactus;
use Modules\Contactus\Requests\ContactRequest;

class ApiController extends Controller
{

    public function send_message(ContactRequest $request)
    {
        Contactus::create(request()->all());
        return api_response(1, __("Message sent successfully"));
    }

    public function send_request(ContactRequest $request)
    {
        Contactus::create(request()->all());
        return api_response(1, __("Request sent successfully"));
    }

    public function contacts()
    {
        $contacts = Setting::contacts()->pluck('value', 'key')->toArray();
        return api_response(1, '', $contacts);
    }

    public function socials()
    {
        $contacts = Setting::socials()->get();
        return api_response(1, '', $contacts);
    }
}
