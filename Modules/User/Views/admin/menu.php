<?php
$menu['User*'] = [
    [
        'title' =>  __("Clients"),
        'link'  =>  '#',
        'icon'  =>  'fas fa-users',
        'childs'    =>  [
            [
                'title' =>  __("Active"),
                'link'  =>  'users.index',
                'query' =>  ['status' => 1]
            ],
            [
                'title' =>  __('Not active'),
                'link'  =>  'users.index',
                'query' =>  ['status' => 0]
            ]
        ]
    ],
    [
        'title' =>  __("Agents"),
        'link'  =>  '#',
        'icon'  =>  'fas fa-users-cog',
        'childs'    =>  [
            [
                'title' =>  __("Active"),
                'link'  =>  'agents.index',
                'query' =>  ['status' => 1]
            ],
            [
                'title' =>  __('Not active'),
                'link'  =>  'agents.index',
                'query' =>  ['status' => 0]
            ]
        ]
    ],
    // [
    //     'title' =>  __('Guests'),
    //     'link'  =>  'guests.index',
    //     'icon'  =>  'fas fa-users'
    // ],
    [
        'title' =>  __("Roles & Moderators"),
        'link'  =>  '#',
        'icon'  =>  'fas fa-user-secret',
        'childs'    =>  [
            [
                'title' =>  __("Roles"),
                'link'  =>  'roles.index'
            ],
            [
                'title' =>  __('Moderators'),
                'link'  =>  'moderators.index'
            ]
        ]
    ],
];
