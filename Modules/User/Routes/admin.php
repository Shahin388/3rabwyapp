<?php
Route::resource('users', 'AdminController');
Route::resource('agents', 'AgentController');
Route::resource('roles', 'RolesController');
Route::resource('moderators', 'ModeratorsController');
