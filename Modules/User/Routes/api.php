<?php
Route::group(['namespace' => 'Api', 'middleware' => 'api'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('forget', 'AuthController@forget');
    Route::post('reset_code', 'AuthController@reset_code');
    Route::post('reset', 'AuthController@reset');
    Route::post('activate', 'AuthController@activate');
    Route::post('social_login', 'AuthController@social_login');


    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('checkPhone', 'AuthController@checkPhone');
        Route::get('profile', 'ApiController@show');
        Route::post('change_password', 'ApiController@change_password');
        Route::post('edit_profile', 'ApiController@update');
        Route::post('userLocation', 'ApiController@userLocation');


        Route::get('notifications', 'ApiController@notifications');
        Route::post('invite_friend', 'ApiController@invite');

        Route::post('rate/{id}', 'ApiController@rate');
        Route::get('rates', 'ApiController@getRates');
        
        Route::post('report/{id}', 'ApiController@report');
        Route::get('reports', 'ApiController@getReports');
    });
});
