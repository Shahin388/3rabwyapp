<?php

namespace Modules\User\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\User\Models\Token;
use Modules\User\Models\User;

class WebController extends Controller
{
    public function login()
    {
        if (request()->isMethod('get')) {
            return view('User::auth.login');
        }
        if (auth()->attempt(request(['email', 'password']), true)) {
            if (auth()->user()->role_id) {
                return redirect()->to('admin');
            }
            auth()->logout();
        }
        return back()->with('error', 'خطأ فى البريد الإلكترونى أو كلمة المرور');
    }

    public function logout()
    {
        auth()->logout();
        auth('stores')->logout();
        return redirect()->to('login');
    }
    public function active($token)
    {
        $token = Token::whereToken($token)->first();
        if (!$token) {
            abort('404');
        }
        $token->user->update(['status' => 1]);
        return view('User::activated');
    }

    public function reset(Request $request, $token)
    {
        $token = Token::whereToken($token)->first();
        if (!$token) {
            abort('404');
        }
        if (request()->isMethod('get')) {
            return view('User::password.reset');
        }
        $this->validate($request, [
            'password' => 'confirmed|required|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
        ]);
        $token->user->update(['password' => request('password')]);
        return redirect()->to('/')->with('success', __("Your password updated successfully"));
    }
}
