<?php

namespace Modules\User\Controllers;

use Modules\Common\Controllers\HelperController;
use Modules\User\Models\User;
use Illuminate\Http\Request;

class AgentController extends HelperController
{
    public function __construct()
    {
        $this->model = new User;
        $this->rows = User::where('role_id', null);
        $this->title = "Agents";
        $this->name =  'agents';
        $this->list = ['name' => 'الاسم', 'email' => 'البريد الإلكترونى', 'mobile' => 'رقم الجوال', 'created_at' => 'تاريخ الإشتراك'];
        $this->inputs = [
            'first_name'  =>  ['title'    =>  'الاسم الأول'],
            'last_name'  =>  ['title'    =>  'الاسم الاخير'],
            'email'  =>  ['title'    =>  'البريد الإلكترونى'],
            'mobile'  =>  ['title'    =>  'رقم الجوال'],
            'password'  =>  ['title'    =>  'كلمة المرور', 'type' => 'password'],
            'image'  =>  ['title'    =>  'الصورة', 'type' => 'image'],
        ];
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'    =>  'required',
            'last_name'     =>  'required',
            'email'   =>  'required|email|unique:users',
            'mobile' =>  'required|unique:users',
            'password'  =>  'required|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
        ]);
        User::create($request->all());
        return response()->json(['url' => route('admin.users.index'), 'message' => 'تم اضافة المستخدم بنجاح']);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'first_name'    =>  'required',
            'last_name'     =>  'required',
            'email'   =>  'required|email|unique:users,email,' . $user->id,
            'mobile' =>  'required|unique:users,mobile,' . $user->id,
        ]);
        if (request('password')) {
            $this->validate($request, ['password'  =>  'min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/']);
        }
        $user->update($request->all());
        return response()->json(['url' => route('admin.users.index'), 'message' => 'تم تعديل بيانات المستخدم بنجاح']);
    }
}
