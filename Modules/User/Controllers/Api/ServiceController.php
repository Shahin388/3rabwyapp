<?php

namespace Modules\User\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Categories\Models\Category;
use Modules\User\Models\Service;
use Modules\User\Models\User;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index']);
    }
    public function index()
    {
        if (!request('barber_id')) return api_response('error', '', ['barber_id' => 'barber_id is required'], 422);
        $barber = User::where(['type' => 'barber', 'id' => request('barber_id')])->firstOrFail();
        $groups = $barber->services()->pluck('category_id')->toArray();
        $categories = Category::whereIn('id', $groups)->get(['id' , 'name']);
        foreach ($categories as $category) {
            $category->services = $barber->services()->where('category_id', $category->id)->get(['id' , 'name' , 'price']);
        }
        return api_response(1, '', $categories);
    }

    public function show($id)
    {
        $service = Service::findOrFail($id);
        return api_response(1, '', $service);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  =>  'required',
            'price' =>  'required',
            'category_id'   =>  'required|exists:categories,id'
        ]);
        $user = auth('api')->user();
        $user->services()->create(request()->all());
        return api_response(1, __("Service added successfully"));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  =>  'required',
            'price' =>  'required',
            'category_id'   =>  'required|exists:categories,id'
        ]);
        $user = auth('api')->user();
        $user->services()->findOrFail($id)->update(request()->all());
        return api_response(1, __("Service updated successfully"));
    }

    public function destroy($id)
    {
        auth('api')->user()->services()->findOrFail($id)->delete();
        return api_response(1, __("Service deleted successfully"));
    }
}
