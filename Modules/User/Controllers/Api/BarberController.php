<?php

namespace Modules\User\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\User\Models\User;
use Modules\User\Resources\BarbersResource;
use Modules\User\Resources\BarberResource;

class BarberController extends Controller
{

    public function index()
    {
        $barbers = User::active()->barber()
            ->when(request('city_id'), function ($query) {
                return $query->where('address->city_id', request('city_id'));
            })->when(request('hai_id'), function ($query) {
                return $query->where('address->hai_id', request('hai_id'));
            })->when(request('keyword'), function ($query) {
                return $query->where('username', 'like', '%' . request('keyword') . '%')
                    ->orWhere('mobile', 'like', '%' . request('keyword') . '%');
            })->get();
        return api_response(1, '', BarbersResource::collection($barbers));
    }

    public function show($id)
    {
        $barber = User::where(['type' => 'barber', 'id' => $id])->firstOrFail();
        return api_response(1, '', new BarberResource($barber));
    }

    public function like($id)
    {
        $user = auth('api')->user();
        $barber = User::findOrFail($id);
        if ($like = $barber->favourites()->where('barber_id', $barber->id)->first()) {
            $like->delete();
            return api_response(1, '', ['like' => 0]);
        }
        $barber = $barber->favourites()->create([
            'user_id'   =>  $user->id
        ]);
        $message = "قام {$user->username} بإضافتك للمفضلة";
        $barber->notifications()->create([
            'barber_id'  =>  $barber->id,
            'model' =>  'barber',
            'text'  =>  $message
        ]);
        $device = $barber->device;
        if ($device) send_fcm($device->device_token, $device->device_type, $message, null, $barber->id);
        return api_response(1, '', ['like' => 1]);
    }

    public function rate(Request $request, $id)
    {
        $barber = User::where(['type' => 'barber', 'id' => $id])->firstOrFail();
        $this->validate($request, [
            'rate'  =>  'required|numeric'
        ]);
        $barber->myrates()
            ->firstOrCreate(['user_id'   =>  auth('api')->user()->id])
            ->update([
                'rate'  =>  request('rate'),
                'text'  =>  request('text')
            ]);
        $message = 'يوجد تقييم جديد لك';
        $barber->notifications()->create([
            'barber_id'  =>  $barber->id,
            'model' =>  'barber',
            'text'  =>  $message
        ]);
        $device = $barber->device;
        if ($device) send_fcm($device->device_token, $device->device_type, $message, null, $barber->id);
        return api_response(1, __("Order rated successfully"));
    }
}
