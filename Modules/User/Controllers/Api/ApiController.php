<?php

namespace Modules\User\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\User\Models\User;
use Modules\User\Models\Report;

use Modules\User\Requests\UserRequest;
use Modules\User\Requests\UserDataRequest;
use Modules\User\Resources\BarbersResource;
use Modules\User\Resources\UserResource;

class ApiController extends Controller
{
    public function show()
    {
        
        $user = auth('api')->user();
        
        $user = User::where('id', $user->id)->first();
        $user->access_token = str_replace('Bearer ', '', request()->header('Authorization'));
        return api_response(1, '', new UserResource($user));
    }


    public function change_password(Request $request)
    {
        $user = auth('api')->user();
        if (!\Hash::check(request('old_password'), $user->password)) {
            return api_response('error', __("Old password not matched"));
        }
        $this->validate($request, [
            'password' => 'required|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
        ]);
        $user->update(['password' => request('password')]);
        return api_response(1, __('Your password changed successfully'));
    }

    public function update(UserDataRequest $request)
    {
        auth('api')->user()->update(request()->except('mobile','gender','email'));
        $user = auth('api')->user();
        if(request('image')){
            $image = request('image');
            $user->setImageAttribute($image);    
        }
        if ($user->type == 'barber') {
            $info  = request()->only(['nationality', 'health', 'hwia', 'about', 'service_type']);
            $info['service_type'] = implode(' - ', $info['service_type']);
            $user->info()->update($info);
        }

        $user->access_token = auth('api')->login($user);
        return api_response(1, __("Profile updated successfully"), new UserResource($user));
    }

    public function favourites()
    {
        $likes = auth('api')->user()->myfavourites()->pluck('barber_id')->toArray();
        $barbers = BarbersResource::collection(User::whereIn('id', $likes)->get());
        return api_response(1, '', $barbers);
    }

    public function notifications()
    {
        $user = auth('api')->user();
        return api_response(1, '', $user->notifications()->whereHas('offer')->get());
    }
    
    
    public function userLocation()
    {
        //$user = auth('api')->user();
        auth('api')->user()->update(request()->all());
         
        return api_response(1, 'user location updated successfully', '');
    }

    public function invite(Request $request)
    {
        $this->validate($request, [
            'mobile'    =>  'required'
        ]);
        if (User::where('mobile', request('mobile'))->exists()) {
            return api_response('error', __('This mobile already registered'));
        }
        $user = auth('api')->user();
        $user->invitations()->firstOrCreate(['mobile' => request('mobile')]);
        send_sms(request('mobile'), __(":user invite you to register in Arbawy delivery app", ['user' => $user->username]));
        return api_response(1, __('Your inviation sent successfully'));
    }


    public function rate(Request $request, $id)
    {
        //$this->validate($request, ['rate' => 'required|numeric']);
        $agent = User::findOrFail($id);
        $user = auth('api')->user();
        $agent->myrates()->firstOrCreate(['user_id' => $user->id])->update([
            'rate'  =>  request('rate'),
            'comment'   =>  request('comment')
        ]);
        return api_response(1, __('Your rate added successfully'));
    }
    
    public function report(Request $request, $id)
    {
        //$this->validate($request, ['rate' => 'required|numeric']);
        $agent = User::findOrFail($id);
        //dd($agent);
        $user = auth('api')->user();
        
         $report = new Report([
        'reported_id' => $agent->id,
        'user_id' =>$user->id,
        'comment' => request('comment')
        ]);
            $report->save();
        
        return api_response(1, __('Your Report added successfully'));
    }
    
     public function getReports(Request $request)
    {
        $user = auth('api')->user();
        return api_response(1, '', $user->reports()->get());
    }
}
