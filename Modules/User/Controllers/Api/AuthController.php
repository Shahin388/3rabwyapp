<?php

namespace Modules\User\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\User\Models\Device;
use Modules\User\Models\User;
use Modules\User\Requests\LoginRequest;
use Modules\User\Requests\UserRequest;
use Modules\User\Resources\UserResource;
use Modules\User\Models\Token;
use Modules\User\Resources\AgentResource;
use Auth;
class AuthController extends Controller
{
    public function signup(UserRequest $request)
    {
        $data = request()->all();
        $user = User::create($data);
        if(request('image')){
            $image = request('image');
            $user->setImageAttribute($image);    
        }
        
        if ($data['type'] == 'agent') {
            session()->put('agent', $user->id);
            $user->info()->create($data);
            $user->car()->create(request('car'));
        }
        if (request('social_id')) {
            return $this->social_login();
        }
        $code = rand(1000, 9999);
        $user->token()->delete();
        $user->token()->firstOrCreate(['token' => $code]);
        $message['title'] = __("كود التسجيل");
        $message['content'] = __("كود تسجيل الدخول : ") . $code;
        send_sms($user->mobile, $message['content']);
        // $this->send_activation_sms($user);
        return api_response(1, __("تم تسجيل حسابك بنجاح .. تم ارسال كود التسجيل الى جوالك"), ['code' => $code]);
    }

    public function social_login()
    {
        $user = User::firstOrCreate(request()->all());
        
        $username = $user->username ?? 'Social User';
        $user->update(['status' => 1, 'username' => $username,'social_type' => request('socail_type'),'social_id'=>request('social_id'),'social_image' => request('social_image')]);
        
        if (request('device_token')) {
            $device = $user->device;
            $device_info = [
                'user_id'   =>  $user->id,
                'device_type'   =>  request('device_type', 'android'),
                'device_token'  =>  request('device_token')
            ];
            if ($device) {
                $device->update($device_info);
            } else {
                Device::create($device_info);
            }
        }
        // $user->token()->delete();
        $user->social_type = request('social_type');
        $user->social_id = request('social_id');
        $user->social_image = request('social_image');

        $user->save();
        $user->access_token = auth('api')->login($user);
        
        // dd($user->type);
        $user = $user->type == 'agent' ? new AgentResource($user) : new UserResource($user);
        return api_response(1, '', $user);
    }
    
    
    public function social_login2()
    {
        //dd(request()->all());
        $user = User::where('social_id',request('social_id'))->first();
        if(isset($user)){
        
            
        
        $user = User::firstOrCreate(request()->all());
        
        $username = $user->username ?? 'Social User';
        $user->update(['status' => 1, 'username' => $username,'social_type' => request('socail_type'),'social_id'=>request('social_id')]);
        
        if (request('device_token')) {
            $device = $user->device;
            $device_info = [
                'user_id'   =>  $user->id,
                'device_type'   =>  request('device_type', 'android'),
                'device_token'  =>  request('device_token')
            ];
            if ($device) {
                $device->update($device_info);
            } else {
                Device::create($device_info);
            }
        }
        // $user->token()->delete();
        $user->social_type = request('social_type');
        $user->social_id = request('social_id');
        $user->save();
        $user->access_token = auth('api')->login($user);
        
        // dd($user->type);
        $user = $user->type == 'agent' ? new AgentResource($user) : new UserResource($user);
        return api_response(1, '', $user);
        
        }else{
            return api_response('error', __("This User Not Found"), null, 401);
        }
    }

    public function login(Request $request)
    {
        if (request('social_id')) {
            return $this->social_login2();
        }
        if ($user = User::where('mobile', request('mobile'))->first()) {
            if ($code = request('code')) {
                $token = $user->token()->where('token', $code)->first();
                if (!$token) {
                    return api_response('error', 'كود التسجيل غير صحيح');
                }
                if (request('device_token')) {
                    $device = $user->device;
                    $device_info = [
                        'user_id'   =>  $user->id,
                        'device_type'   =>  request('device_type', 'android'),
                        'device_token'  =>  request('device_token')
                    ];
                    if ($device) {
                        $device->update($device_info);
                    } else {
                        Device::create($device_info);
                    }
                }
                // $user->token()->delete();
                $user->access_token = auth('api')->login($user);
                // dd($user->type);
                $user = $user->type == 'agent' ? new AgentResource($user) : new UserResource($user);
                return api_response(1, '', $user);
            } else {
                $code = rand(1000, 9999);
                $user->token()->delete();
                $user->token()->firstOrCreate(['token' => $code]);
                $message['title'] = __("كود التسجيل");
                $message['content'] = __("كود تسجيل الدخول : ") . $code;
                send_sms($user->mobile, $message['content']);
                return api_response(1, 'تم ارسال كود التسجيل الى جوالك', ['code' => $code]);
            }
        }
        return api_response('error', __("Invalid credintials"), null, 401);
    }

    public function activate()
    {
        $code = request('code');
        if (!$code) {
            return api_response('error', '', ['code' => 'code is required'], 422);
        }
        $code = Token::whereToken($code)->first();
        if (!$code) {
            return api_response('error', __("Code is not correct"));
        }
        $code->user()->update(['status' => 1]);
        $user = $code->user;
        $user->access_token = auth('api')->login($user);
        return api_response(1, __("Your account activated successfully"), new UserResource($user));
    }

    public function forget()
    {
        if (!request('mobile')) {
            return api_response('error', '', ['mobile' => 'mobile field is required'], 422);
        }
        $user = User::whereMobile(request('mobile'))->first();
        if (!$user) {
            return api_response('error', __("Mobile not found"));
        }
        $code = rand(1000, 9999);
        $user->token()->firstOrCreate(['token' => $code]);
        $message['title'] = __("Reset password");
        $message['content'] = __("Your code to reset password is : ") . $code;
        send_sms($user->mobile, $message['content']);

        return api_response(1, __("Reset password code sent to your mobile"), ['code' => $user->token->token]);
    }

    public function reset_code(Request $request)
    {
        $this->validate($request, ['code' => 'required|exists:user_tokens,token']);
        $code = Token::whereToken(request('code'))->first();
        return api_response(1, '', ['code_id' => $code->id]);
    }

    public function reset(Request $request)
    {
        $code = Token::findOrFail(request('code_id'));
        $this->validate($request, [
            'password' => 'required|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
        ]);
        User::find($code->user_id)->update(['password' => request('password')]);
        $code->delete();
        return api_response(1, __('Your password changed successfully'));
    }
    
     public function checkPhone(Request $request)
    {
        $phone = request('phone');
        $user = User::where('mobile',$phone)->first();
        //dd($user);
        if(isset($user)){
            return api_response(1, 'phone is already exist','');
        }else{
            
            $code = rand(1000, 9999);
            $user = Auth::user();
            //dd($user);
            $user->token()->delete();
            $user->token()->firstOrCreate(['token' => $code]);
            //dd($user);
            // if(request('code')){
            //     $code = Token::whereToken(request('code'))->first();
            // if (!$code) {
            //     return api_response('error', __("Code is not correct"));
            // }
            // $code->user()->update(['status' => 1]);
            // $user = $code->user;
            // $user->access_token = auth('api')->login($user);
            // return api_response(1, __("Your account activated successfully"), new UserResource($user));
            // }
            $message['title'] = __("كود التسجيل");
            $message['content'] = __("كود تسجيل الدخول : ") . $code;
            send_sms($user->mobile, $message['content']);
            return api_response(1, 'phone is not exist and sent code to your mobile',['code'=>$code]);

        }
    }

    private function send_mail($user, $message)
    {
        $message = json_decode(json_encode($message));
        \Mail::send('User::emails.default', ['user' => $user, 'msg' => $message], function ($mail) use ($user, $message) {
            $mail->to($user->email, $user->name)->subject($message->title);
        });
    }


    private function send_activation_sms($user)
    {
        $code = rand(1000, 9999);
        $user->token()->firstOrCreate(['token' => $code]);
        $message['title'] = __("Activation code");
        $message['content'] = __("Your activation code is : ") . $code;

        send_sms($user->mobile, $message['content']);
        // $message['button_txt'] = __("Active account");
        // $message['button_link'] = route('user.active', $user->token->token);
        // $this->send_mail($user, $message);
    }
}
