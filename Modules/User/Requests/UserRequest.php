<?php

namespace Modules\User\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\User\Models\User;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth('api')->user() ?? new User;
        $rules =  [
            'username'    =>  'required',
            'email'   =>  'email|unique:users,email,' . $user->id,
            'gender'    =>  'required|in:male,female',
            'mobile' => 'required|unique:users,mobile,' . $user->id

        ];
        if (request('social_id')) {
            unset($rules['username'], $rules['gender']);
            $rules['social_id'] = 'required|unique:users,social_id,' . $user->id;
            $rules['social_type'] = 'required';
        } elseif (request('type') == 'agent') {
            $rules['mobile'] = 'required|unique:users,mobile,' . $user->id;
            $rules['national_id'] = 'required';
            $rules['national_id_image'] = 'required';
            $rules['car'] = 'required|array';
            $rules['work_type'] = 'required|array';
        }
        if (!$user->id) {
           // $rules['type']  =  'required|in:client,agent';
            // $rules['password'] = 'required|min:6';
        }
        return $rules;
    }
}
