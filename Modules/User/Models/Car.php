<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $table = 'user_cars';
    protected $fillable = [
        'type_id',
        'brand_id',
        'license',
        'front',
        'back',
        'production_year'
    ];

    public function setLicenseAttribute($image)
    {
        $this->attributes['license'] = $image->store('uploads/agents/' . session('agent', 'unknown'));
    }

    public function setFrontAttribute($image)
    {
        $this->attributes['front'] = $image->store('uploads/agents/' . session('agent', 'unknown'));
    }

    public function setBackAttribute($image)
    {
        $this->attributes['back'] = $image->store('uploads/agents/' . session('agent', 'unknown'));
    }
}
