<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Products\Models\Product;

class Favourite extends Model
{
    protected $fillable = ['user_id', 'product_id'];

    protected $table = 'favourites';

    public function product(){
        return $this->belongsTo(Product::class , 'product_id');
    }


    public function user(){
        return $this->belongsTo(User::class , 'user_id');
    }
}
