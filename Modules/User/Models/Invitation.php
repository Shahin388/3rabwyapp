<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $table = "invitations";
    protected $fillable = ['mobile'];
}
