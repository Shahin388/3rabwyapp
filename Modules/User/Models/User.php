<?php

namespace Modules\User\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Addresses\Models\Address;
use Modules\Areas\Models\Area;
use Modules\Areas\Models\City;
use Modules\Areas\Models\Hai;
use Modules\Categories\Models\Place;
use Modules\Common\Models\Notification;
use Modules\Copons\Models\Copon;
use Modules\Orders\Models\Cart;
use Modules\Orders\Models\Order;
use Modules\Orders\Models\Purchase;
use Modules\Products\Models\Product;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'mobile',
        'password',
        'image',
        'status',
        'role_id',
        'type',
        'address',
        'area_id',
        'whatsapp',
        'gender',
        'birth_date',
        'lat',
        'lang',
        'socail_id',
        'socail_type',
        'social_image',
        'copon_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = ['address' => 'array'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    public function car()
    {
        return $this->hasOne(Car::class, 'user_id');
    }
    public function info()
    {
        return $this->hasOne(Info::class, 'user_id');
    }

    public function area()
    {
        return $this->belongsTo(Area::class)->select('id', 'name');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeAgent($query)
    {
        return $query->where('type', 'agent');
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function setImageAttribute($image)
    {
        $this->attributes['image'] = $image->store("uploads/users");
    }

    public function getImageAttribute($image)
    {
        return $image ? url($image) : url('placeholders/user.png');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class)->latest();
    }

    public function favourites()
    {
        return $this->hasMany(Favourite::class, 'agent_id');
    }

    public function myfavourites()
    {
        return $this->hasMany(Favourite::class);
    }

    public function getIsLikedAttribute()
    {
        $user = auth('api')->user();
        if ($user) {
            return $user->myfavourites()->where('agent_id', $this->id)->exists();
        }
        return false;
    }

    public function rates()
    {
        return $this->hasMany(Rate::class);
    }
    
     public function reports()
    {
        return $this->hasMany(Report::class);
    }

    public function myrates()
    {
        return $this->morphMany(Rate::class, 'rated')->select('id', 'user_id', 'rate');
    }
    public function getRateUsersAttribute()
    {
        return $this->rates()->count();
    }

    public function getRateAttribute()
    {
        $myrates_count = $this->myrates()->count();
        $myrates = $this->myrates()->sum('rate');
        if ($myrates_count) {
            return ($myrates / (5 * $myrates_count)) * 5;
        }
        return 0;
    }

    public function getFullAddressAttribute()
    {
        $address = $this->address;
        if ($address && isset($address['city_id'])) {
            $city = City::find($address['city_id'])->name ?? '';
            $hai = Hai::find($address['hai_id'])->name ?? '';
            return $city . ' - ' . $hai;
        }
        return '';
    }

    public function getAddressAttribute($address)
    {
        $address = json_decode($address);
        return [
            'city_id'   =>  $address->city_id ?? null,
            'hai_id'    =>  $address->hai_id ?? null,
            'latitude'  =>  $address->latitude ?? null,
            'longitude'  =>  $address->longitude ?? null,
            'text'  =>  $address->text ?? ''
        ];
    }

    public function token()
    {
        return $this->hasOne(Token::class, 'user_id');
    }

    public function device()
    {
        return $this->hasOne(Device::class);
    }

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'agent_id');
    }

    public function myorders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    public function places()
    {
        return $this->hasMany(Place::class, 'user_id');
    }

    public function invitations()
    {
        return $this->hasMany(Invitation::class);
    }

    public function getCreatedAtAttribute($created_at)
    {
        return date('Y-m-d', strtotime($created_at));
    }

    public function copon()
    {
        return $this->belongsTo(Copon::class, 'copon_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function claims()
    {
        return $this->hasMany(Claim::class, 'user_id');
    }

    public function myclaims()
    {
        return $this->hasMany(Claim::class, 'agent_id');
    }

    public function getRoleNameAttribute()
    {
        return $this->role->name ?? '#';
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
