<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $table = 'user_info';
    protected $fillable = [
        'national_id',
        'national_id_image',
        'work_type',
        'how_know',
        'driver_license'
    ];

    protected $casts = ['work_type' => 'array'];

    public function setNationalIdIamgeAttribute($image)
    {
        $this->attributes['national_id_image'] = $image->store('uploads/agents/' . session('agent', 'unknown'));
    }

    public function setDriverLicenseAttribute($image)
    {
        $this->attributes['driver_license'] = $image->store('uploads/agents/' . session('agent', 'unknown'));
    }
}
