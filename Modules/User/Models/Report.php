<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';
    protected $fillable = ['user_id', 'reported_id' , 'comment'];
    protected $with = ['user'];

    

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->select('id' , 'username' ,'image');
    }
}
