<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'rates';
    protected $fillable = ['user_id', 'rate' , 'comment'];
    protected $with = ['user'];

    public function rated()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->select('id' , 'username' ,'image');
    }
}
