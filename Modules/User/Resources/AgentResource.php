<?php

namespace Modules\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Areas\Models\City;
use Modules\Areas\Models\Hai;

class AgentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'type'  =>  $this->type,
            'username'  =>  $this->username,
            'mobile'   =>   $this->mobile,
            'email' =>  $this->email,
            'image' =>  $this->image,
            'whatsapp'   =>  $this->whatsapp,
            'hwia'  =>  $this->hwia ?? '',
            'is_liked'  =>  $this->is_liked,
            'rate'  =>  $this->rate,
            'area' => $this->area,
            'social_image' => $this->social_image,
            'access_token'  =>  $this->access_token
        ];
    }
}
