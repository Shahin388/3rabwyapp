<?php

namespace Modules\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AgentsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'type'  =>  $this->type,
            'username'  =>  $this->username,
            'image' =>  $this->image,
            'is_liked'  =>  $this->is_liked,
            'address'   =>  $this->full_address,
            'nationality'   =>  $this->info->nationality ?? '',
            'service_type'  =>  $this->info->service_type ?? '',
            'rate'  =>  $this->rate
        ];
    }
}
