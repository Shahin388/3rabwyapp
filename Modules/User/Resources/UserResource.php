<?php

namespace Modules\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'type'  =>  $this->type,
            'username'  =>  $this->username,
            'mobile'   =>   $this->mobile,
            'email' =>  $this->email,
            'image' =>  $this->image,
            'delivery_fees' => $this->delivery_fees ?? 0,
            'credits'   =>  $this->credits ?? 0,
            'orders_count'  =>  $this->orders_count ?? 0,
            'trips_count'   =>  $this->trips_count ?? 0,
            'whatsapp'   =>  $this->whatsapp,
            'hwia'  =>  $this->hwia,
            'area' => $this->area,
            'social_id' => $this->social_id,
            'social_type' => $this->social_type,
            'social_image' => $this->social_image,
            'birth_date' => $this->birth_date,
            'rates_count'   => $this->myrates()->count(),
            'claims_count'  => $this->myclaims()->count() ?? 0,
            'copon_id'  =>  $this->copon->code ?? null,
            'access_token'  =>  $this->access_token
        ];
    }
}
