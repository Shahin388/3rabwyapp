@extends('Common::admin.layout.page')
@section('page')

<div class="row">
    <div class="col-md-6 col-sm-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('Order Details')</h3>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-stripped">
                    <tbody>
                        <tr>
                            <th>@lang("Order ID")</th>
                            <td>{{ $order->id }}</td>
                        </tr>
                        <tr>
                            <th>@lang('Status')</th>
                            <td>
                                <select name="status" class="form-control order_status">
                                    @foreach(order_statuses() as $status)
                                    <option {{ $order->status == $status ? 'selected' : '' }} value="{{ $status }}">
                                        @lang($status)</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>@lang('Total')</th>
                            <td>{{ '#' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('Agent')</th>
                            <td>{{ $order->agent->username ?? '#' }}</td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('Client information')</h3>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>@lang('Client name')</th>
                            <td>{{ $order->user->username }}</td>
                        </tr>
                        <tr>
                            <th>@lang('Phone') </th>
                            <td>{{ $order->user->mobile }}</td>
                        </tr>
                        <tr>
                            <th>@lang('Email')</th>
                            <td>{{ $order->user->email }}</td>
                        </tr>
                        <tr>
                            <th>@lang('Address')</th>
                            <td>{{ $order->user->full_address }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    
</div>
<script>
    $('body').on('change', '.order_status', function () {
        $.get("{{ route('admin.orders.status' , $order->id) }}", {
            status: $(this).val()
        }, function (result) {
            Swal.fire({
                text: result.message
            });
        });
    });

</script>
@stop
