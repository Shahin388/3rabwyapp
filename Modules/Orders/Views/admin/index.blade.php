@extends('Common::admin.layout.page')
@section('page')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ __($title) }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>@lang("Order ID")</th>
                            <th>@lang('Client')</th>
                            <th>@lang('Barber')</th>
                            <th>@lang('Total')</th>
                            <th>@lang('Created at')</th>
                            <th>@lang('Show')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $row)
                        <tr>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->user->username ?? $row->guest->name ?? '#' }}</td>
                            <td>{{ $row->agent->username ?? '#' }}</td>
                            <td>{{ '#' }} KW</td>
                            <td>{{ $row->payment_method }}</td>
                            <td class="actions_td">
                                <a class="btn btn-primary mlink" href="{{ route("admin.orders.show" , $row->id) }}">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $orders->links() }}
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<script>
    // $('.table').DataTable({
    //     "paging": false,
    //     "lengthChange": false,
    //     "searching": true,
    //     "ordering": true,
    //     "info": true,
    //     "autoWidth": false,
    // });

</script>

@stop
