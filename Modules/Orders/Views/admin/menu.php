<?php

$menu['Orders*'] = [
    [
        'title' =>  __("Inprogress"),
        'link'  =>  'orders.index',
        'icon'  =>  'fas fa-th-list',
        'query' =>  ['status' => 'inprgress']
    ],
    [
        'title' =>  __("Completed"),
        'link'  =>  'orders.index',
        'icon'  =>  'fas fa-check',
        'query' =>  ['status' => 'done']
    ],
    [
        'title' =>  __("Pending"),
        'link'  =>  'orders.index',
        'icon'  =>  'fas fa-th-list',
        'query' =>  ['status' => 'pending']
    ],
];
