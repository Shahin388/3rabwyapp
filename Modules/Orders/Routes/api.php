<?php
Route::post('orderPlaces', 'Api\ApiController@orderPlaces');

Route::group(['middleware' => 'auth:api', 'namespace' => 'Api', 'middleware' => 'auth:api'], function () {
    Route::get('pending_orders', 'ApiController@pending_orders');
    Route::get('orders', 'ApiController@index');
    Route::post('orders', 'ApiController@store');
    Route::get('orders/{id}', 'ApiController@show');
    Route::get('order/{id}/history', 'ApiController@history');
    Route::post('order/{id}/message', 'ApiController@message');
    Route::get('order/{id}/done', 'ApiController@done');
    Route::get('order/{id}/cancel', 'ApiController@cancel');
    Route::post('order/{id}/rate', 'ApiController@rate');

    Route::post('order/{id}/bill', 'OfferController@bill');
    Route::post('order/{id}/send_offer', 'OfferController@send_offer');
    Route::get('order/{id}/offers', 'OfferController@offers');
    Route::get('order/{order_id}/offers/{offer_id}', 'OfferController@show');
    Route::get('order/{order_id}/accept_offer/{offer_id}', 'OfferController@accept_offer');
});
