<?php
Route::resource('orders', 'AdminController')->only("index", "show", 'destroy');
Route::get('orders/{id}/status', 'AdminController@status')->name('orders.status');

Route::resource('guests', 'GuestController')->only("index", "show", 'destroy');
