<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->boolean('seen')->nullable();
            $table->string('status', 20)->default('pending');
            $table->string('service_type')->default('صالون');
            $table->unsignedBigInteger('user_id')->index()->nullable();
            $table->unsignedBigInteger('guest_id')->index()->nullable();
            $table->json('services')->nullable();
            $table->float('total');
            $table->float('delivery_price');
            $table->float('package_price');
            $table->text('notes')->nullable();
            $table->date('order_date')->nullable();
            $table->string('order_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
