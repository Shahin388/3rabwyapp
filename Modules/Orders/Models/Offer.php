<?php

namespace Modules\Orders\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;

class Offer extends Model
{
    protected $table = "order_offers";
    protected $fillable = ['status', 'agent_id', 'order_id', 'fees'];
    protected $hidden = ['updated_at'];

    public function agent()
    {
        return $this->belongsTo(User::class, 'agent_id')->select('id', 'username', 'image');
    }

    public function user()
    {
        return $this->belongsTo(User::class)->select('id', 'username', 'mobile');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function getDistanceAttribute()
    {
        $delivery = $this->order->delivery_location;
        return distance($delivery['lat'], $delivery['lng'], request()->header('Lat'), request()->header('Lng'), "K");
    }

    public function getArrivalTimeAttribute()
    {
        $distance = (float) str_replace(["km", ","], ["", "."], $this->distance);
        return $distance * 5 . " min";
    }
}
