<?php

namespace Modules\Orders\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Categories\Models\Place;
use Modules\User\Models\User;
use Modules\User\Models\Rate;

class Order extends Model
{
    protected $table = "orders";
    protected $fillable = [
        'status',
        'total',
        'agent_id',
        'user_id',
        'seen',
        'notes',
        'image',
        'audio',
        'delivery_name',
        'receipt_name',
        'delivery_address',
        'receipt_address',
        'copon_id',
        'place_id',
        'delivery_location',
        'receipt_location',
        'delivery_time',
        'delivery_price',
        'package_price',
        'created_at'
    ];

    protected $casts = [
        'delivery_location' => 'array',
        'receipt_location' => 'array',
        'total' => 'array'
    ];

    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id', 'place_id');
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function history()
    {
        return $this->hasMany(OrderHistory::class, 'order_id');
    }

    public function scopeUnseen($query)
    {
        return $query->where('seen', null);
    }

    public function agent()
    {
        return $this->belongsTo(User::class, 'agent_id')->select('id', 'username', 'image');
    }

    public function user()
    {
        return $this->belongsTo(User::class)->select('id', 'username', 'image');
    }

    public function rate()
    {
        return $this->morphOne(Rate::class, 'rated')->select('id', 'user_id', 'rate');
    }

    public function getBadgeAttribute()
    {
        $status = [
            'done'  =>  'success',
            'canceled'  =>  'danger',
            'pending'   =>  'warning',
            'inpregress'   =>  'warning',
        ];
        return $status[$this->status] ?? '';
    }

    public function getCreatedAtAttribute($created_at)
    {
        return date('Y-m-d', strtotime($created_at));
    }

    public function getTimeAgoAttribute()
    {
        $date = Carbon::parse($this->getOriginal($this->created_at));
        return $date->diffForHumans();
    }

    public function getDeliveryDistanceAttribute()
    {
        $mylat = request()->header('Lat');
        $mylng = request()->header('Lng');
        $delivery_location = $this->delivery_location;
        return distance($mylat, $mylng, $delivery_location['lat'], $delivery_location['lng'], "K");
    }

    public function getReceiptDistanceAttribute()
    {
        $mylat = request()->header('Lat');
        $mylng = request()->header('Lng');
        $receipt_location = $this->receipt_location;
        return distance($mylat, $mylng, $receipt_location['lat'], $receipt_location['lng'], "K");
    }
}
