<?php

namespace Modules\Orders\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    protected $table = 'orders_history';
    protected $fillable = [
        'order_id',
        'sender_id',
        'content',
        'type'
    ];

    protected $appends = ['sent', 'sent_at'];
    protected $hidden = ['created_at', 'updated_at'];

    public function getSentAttribute()
    {
        $user = auth('api')->user();
        if ($user && $user->id == $this->sender_id) return true;
        return false;
    }

    public function getSentAtAttribute()
    {
        return date('h:i a', strtotime($this->created_at));
    }

    public function getContentAttribute($content)
    {
        if (in_array($this->type, ['store_location', 'client_location'])) return (array) json_decode($content);
        elseif ($this->type == 'image') return url($content);
        return $content;
    }

    public function setContentAttribute($content)
    {
        if (is_uploaded_file($content)) {
            $this->attributes['type'] = 'image';
            $this->attributes['content'] = $content->store('uploads/orders');
        } else {
            $this->attributes['content'] = $content;
        }
    }
}
