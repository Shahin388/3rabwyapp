<?php

namespace Modules\Orders\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Categories\Models\Place;
use Modules\Orders\Models\Order;
use Modules\Orders\Models\OrderHistory;
use Modules\Orders\Resources\MyOrdersResource;
use Modules\Orders\Resources\OrderResource;
use Modules\Orders\Resources\OrdersResource;
use Modules\Orders\Resources\UserOrdersResource;
use Modules\User\Models\Device;

class ApiController extends Controller
{
    public function index()
    {
        $user = auth('api')->user();
        if ($user->type == 'agent') {
            if ($status = request('status')) {
                if ($status == 'active') {
                    $orders = $user->orders()->latest()->get();
                } else {
                    $orders = $user->orders()->latest()->get();
                }
            } else {
                $orders = $user->orders()->latest()->get();
            }
            $orders = UserOrdersResource::collection($orders);
        } else {
            if ($status = request('status')) {
                if ($status == 'active') {
                    $orders = $user->myorders()->latest()->get();
                } else {
                    $orders = $user->myorders()->latest()->get();
                }
            } else {
                $orders = $user->myorders()->latest()->get();
            }
            $orders = UserOrdersResource::collection($orders);
        }
        
        return api_response(1, '', $orders);
    }

    public function store(Request $request)
    {
       // dd($request->all());
        $this->validate($request, [
            'receipt_location'  =>  'array',
            'delivery_location'  =>  'required|array',
            'delivery_time' =>  'required',
            'place_id'  =>  'exists:places,place_id',
            'notes' =>  'required'
        ]);
        $data = request()->all();
        if (!request('receipt_location')) {
            $place = Place::where('place_id', request('place_id'))->first();
            $data['receipt_location'] = $place->info['location'];
        }
        $user = auth('api')->user();
        if(request('image')){
                $file = request('image');
                $destinationPath = public_path('uploads');
                $name=$file->getClientOriginalName();
                //dd($name);
                $file->move($destinationPath, $name);
                $data['image'] = $name;
            
        }
        
        if(request('audio')){
                $file = request('audio');
                $destinationPath = public_path('uploads');
                $name=$file->getClientOriginalName();
                //dd($name);
                $file->move($destinationPath, $name);
                $data['audio'] = $name;
            
        }
        
        if(request('copon_id')){
                $data['copon_id'] = request('copon_id');
        }
        
        if(request('delivery_name')){
                $data['delivery_name'] = request('delivery_name');
        }
        
        if(request('receipt_name')){
                $data['receipt_name'] = request('receipt_name');
        }
        if(request('delivery_address')){
                $data['delivery_address'] = request('delivery_address');
        }
        
        if(request('receipt_address')){
                $data['receipt_address'] = request('receipt_address');
        }
        $order = $user->myorders()->create($data);
        
        $created_at = date('Y-m-d H:i:s');
        OrderHistory::insert([
            [
                'order_id'  =>  $order->id,
                'sender_id' => $user->id,
                'content'  => request('notes'),
                'type'  =>  'text',
                'created_at'    =>  $created_at
            ],
            [
                'order_id'  =>  $order->id,
                'sender_id' => $user->id,
                'type'  =>  'store_location',
                'content'   =>  json_encode($order->receipt_location),
                'created_at'    =>  $created_at
            ],
            [
                'order_id'  =>  $order->id,
                'sender_id' => $user->id,
                'type'  =>  'client_location',
                'content'   =>  json_encode($order->delivery_location),
                'created_at'    =>  $created_at
            ]
        ]);
        $this->notify_agents($order, $data['receipt_location']);
        return api_response(1, __('Order created successfully'), ['order_id' => $order->id]);
    }

    public function show($id)
    {
        $user = auth('api')->user();
        //dd($user);
        $order = Order::findOrFail($id);
        return api_response(1, '', new OrderResource($order));
    }

    public function history($id)
    {
        $order = Order::findOrFail($id);
        $data['order'] = [
            'id'    =>  $order->id,
            'user'  =>  $order->user,
            'rate'  =>  $order->user->rate ?? 3
        ];
        $data['history'] = $order->history;
        return api_response(1, '', $data);
    }

    private function notify_agents($order, $location)
    {
        $lat = $location['lat'];
        $lon = $location['lng'];
        $users = \DB::table("user_location")
            ->select(
                "user_id",
                \DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
        * cos(radians(lat)) 
        * cos(radians(lng) - radians(" . $lon . ")) 
        + sin(radians(" . $lat . ")) 
        * sin(radians(lat))) AS distance")
            )
            ->pluck('user_id')->toArray();
        $android_devices = Device::whereIn('user_id', $users)->where('device_type', 'android')->pluck('device_token')->toArray();
        $ios_devices = Device::whereIn('user_id', $users)->where('device_type', 'ios')->pluck('device_token')->toArray();

        $message = "There is new order nearby you";
        send_fcm($android_devices, 'android', $message, $order->id);
        send_fcm($ios_devices, 'ios', $message, $order->id);
    }


    public function pending_orders()
    {
        $orders = Order::where('status', 'pending')->latest()->get();
        return api_response(1, '', OrdersResource::collection($orders));
    }
    
    public function orderPlaces()
    {
        $orders = Order::where('place_id', request('place_id'))->latest()->get();
        return api_response(1, '', OrdersResource::collection($orders));
    }

    public function message(Request $request, $id)
    {
        $this->validate($request, [
            'content'   =>  'required'
        ]);
        $order = Order::findOrFail($id);
        $user = auth('api')->user();
        $order->history()->create(['content' => request('content'), 'sender_id' => $user->id]);
        return api_response(1, __("Message sent successfully"));
    }

    public function done($id)
    {
        $order = Order::findOrFail($id);
        $order->update(['status' => 'done']);
        return api_response(1, __("Order status changed successfully"));
    }

    public function cancel($id)
    {
        $order = Order::findOrFail($id);
        $order->update(['status' => 'canceled']);
        return api_response(1, __("Order canceled successfully"));
    }

    public function rate(Request $request, $id)
    {
        $this->validate($request, [
            'rate'  =>  'required|min:1|max:5'
        ]);
        $data = $request->all();
        $order = Order::findOrFail($id);
        $order->agent->myrates()->create([
            'user_id'   =>  auth('api')->user()->id,
            'rate'  =>  $data['rate'],
            'text'  =>  $data['text']
        ]);
        return api_response(1, __('Your rate send successfully'));
    }
}
