<?php

namespace Modules\Orders\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Orders\Models\Order;
use Modules\Orders\Resources\OfferResource;
use Modules\User\Models\Device;

class OfferController extends Controller
{
    public function send_offer(Request $request, $id)
    {
        $this->validate($request, [
            'fees'  =>  'required'
        ]);
        $user = auth('api')->user();
        $order = Order::findOrFail($id);
        $offer = $order->offers()->firstOrCreate([
            'agent_id'  =>  auth('api')->user()->id,
        ]);
        $offer->update(['fees'  =>  request('fees')]);
        $order->user->notifications()->create([
            'type'  =>  'offer',
            'offer_id'  =>  $offer->id,
            'text'  =>  "There is an offer from :user"
        ]);
        $message = __("There is an offer from :user", ['user' => $user->username]);
        $device = $order->user->device ?? new Device;
        //dd($device);
        send_fcm([$device->device_token], $device->device_type, $message, $order->id);
        return api_response(1, __("Offer sent successfully please wait client approve"));
    }

    public function offers($id)
    {
        $order = Order::findOrFail($id);
        $offers = $order->offers()->with('agent')->latest()->get();
        return api_response(1, '', $offers);
    }

    public function show($order_id, $offer_id)
    {
        $offer = Order::findOrFail($order_id)->offers()->findOrFail($offer_id);
        $offer->agent = $offer->agent;
        return api_response(1, '', new OfferResource($offer));
    }

    public function accept_offer($order_id, $offer_id)
    {
        $order = Order::findOrFail($order_id);
        $offer = $order->offers()->find($offer_id);
        $offer->status = 'approved';
        $offer->save();

        $order->agent_id = $offer->agent_id;
        $order->total = $offer->fees;
        $order->delivery_price = $offer->fees;
        $order->status = 'inprogress';
        $order->save();

        $order->history()->create([
            'sender_id' =>  auth('api')->user()->id,
            'order_id'  =>  $order->id,
            'content'  =>  __('Offer accepted successfully')
        ]);

        $device = $offer->agent->device ?? new Device;
        $message = __("Your offer approved from client");
        send_fcm([$device->device_token], $device->device_type, $message, $order->id);

        return api_response(1, __("Offer accepted successfully"));
    }

    public function bill(Request $request, $id)
    {
        $this->validate($request, [
            'delivery_price'    =>  'required|numeric',
            'package_price'    =>  'required|numeric'
        ]);
        $user = auth('api')->user();
        $order = Order::findOrFail($id);
        $data = request()->only(['delivery_price', 'package_price']);
        $taxs = (float) app_setting('taxs');
        $data['total'] = array_sum($data) + $taxs;
        $order->update($data);

        $total = $taxs + $data['total'];

        $bill = "سعر الطلب {$data['package_price']} \n
        سعر التوصيل {$data['delivery_price']} \n
        الضريبة {$taxs} \n
        الإجمالي {$total} \n";

        $order->history()->create([
            'sender_id'   =>  $user->id,
            'type'  =>  'text',
            'content'   =>  $bill
        ]);
        return api_response(1, __("Bill added successfully"));
    }
}
