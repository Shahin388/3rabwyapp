<?php

namespace Modules\Orders\Controllers;

use App\Http\Controllers\Controller;
use Modules\Orders\Models\Order;

class WebController extends Controller
{

    public function checkout_callback($status)
    {
        if ($status == 'payment_fail') {
            // return redirect()->to('/')->with('error', __('Failed payment'));
            return view('Orders::success', ['message' => 'Failed payment']);
        }
        $order = Order::findOrFail(request('OrderID'));
        $order->status = 'pending';
        $order->payment = request()->all();
        $order->save();
        $order->myuser->cart()->delete();

        $order->myuser->notifications()->create([
            'order_id'  =>  $order->id,
            'text'  =>  "Your order #:num status changed to {$order->status}",
            'model'  =>  'order'
        ]);
        $token = $order->myuser->device->device_token ?? $order->myuser->fb_token ?? '';
        $platform = $order->myuser->device->device_type ?? $order->myuser->platform ?? 'android';
        $message = __("Your order #:num status changed to {$order->status}", ['num' => $order->id]);
        send_fcm($token, $platform, $message, $order->id);
        $order->notifications()->create([
            'user_id' => $order->user_id,
            'text' => __(
                "Successfull payment , your order pending to be transfered"
            )
        ]);
        // return redirect()->to('/')->with('success', __('Successfull payment'));
        return view('Orders::success', ['message' => __('Successfull payment')]);
    }
}
