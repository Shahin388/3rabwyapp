<?php

namespace Modules\Orders\Controllers;

use App\Http\Controllers\Controller;
use Modules\Orders\Models\Order;

class AdminController extends Controller
{
    public function index()
    {
        $orders = auth('stores')->check() ? Order::forStore() : new Order;
        $orders = $orders->when(request('status'), function ($query) {
            return $query->where('status', request('status'));
        })->latest()->paginate(25);
        switch (request('status')) {
            case 'inprogress':
                $title = 'Inprogress';
                break;
            case 'pending':
                $title = "Pending";
                break;
            case 'done':
                $title = "Done";
                break;
            default:
                $title = "Orders";
                break;
        }
        return view('Orders::admin.index', get_defined_vars());
    }

    public function show($id)
    {
        $order = auth('stores')->check() ? Order::forStore()->findOrFail($id) : Order::findOrFail($id);
        $title = __("Order ID") . " #" . $order->id;
        $order->update(['seen' => 1]);
        return view('Orders::admin.show', get_defined_vars());
    }

    public function status($id)
    {
        $this->validate(request(), [
            'status'    =>  'required'
        ]);
        $status = request('status');
        $order = Order::findOrFail($id);
        $order->update(['status' => $status]);
        // $order->myuser->notifications()->create([
        //     'order_id'  =>  $order->id,
        //     'text'  =>  "Your order #:num status changed to $status",
        //     'model'  =>  'order'
        // ]);

        // $token = $order->myuser->device->device_token ?? $order->myuser->fb_token ?? '';
        // $platform = $order->myuser->device->device_type ?? $order->myuser->platform ?? 'android';
        // $message = __("Your order #:num status changed to $status", ['num' => $order->id]);
        // send_fcm($token, $platform, $message, $order->id);

        return response()->json([
            'url' => route('admin.orders.show', $order->id),
            'message' => __('Order status changed succcessfully')
        ]);
    }
}
