<?php

namespace Modules\Orders\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\User\Resources\AgentResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'status' => __($this->status),
            'start_time'    =>  date('d M - h:i a', strtotime($this->created_at)),
            'delivery_time' =>  $this->delivery_time,
            'total' =>  $this->total ?? 0,
            'receipt_location'  =>  $this->receipt_location,
            'delivery_location' => $this->delivery_location,
            'delivery_name'  =>  $this->delivery_name,
            'receipt_name' => $this->receipt_name,
            'delivery_address'  =>  $this->delivery_address,
            'receipt_address' => $this->receipt_address,
            'agent' =>  $this->agent
        ];
    }
}
