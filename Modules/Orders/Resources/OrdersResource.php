<?php

namespace Modules\Orders\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'status'    =>  $this->status,
            'place'    =>  $this->place,
            'user'    =>  $this->user,
            'created_at'    =>  date('Y/m/d', strtotime($this->created_at)),
            'note'  =>  $this->notes,
            'delivery_distance' =>  $this->delivery_distance,
            'receipt_distance' =>  $this->receipt_distance,
            'delivery_time' =>  $this->delivery_time
        ];
    }
}
