<?php

namespace Modules\Orders\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'agent' =>  $this->agent()->first()->append('rate' , 'rate_users'),
            'distance'  =>  $this->distance,
            'arrival_time'  =>  $this->arrival_time,
            'fees'  =>  $this->fees
        ];
    }
}
