<?php

namespace Modules\Orders\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MyOrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'created_at'    =>  date('Y/m/d', strtotime($this->created_at)),
            'receipt_location'  =>  $this->receipt_location,
            'delivery_location' => $this->delivery_location,
            'total' =>  $this->total ?? 0,
            'currency'  =>  __("EGP")
        ];
    }
}
