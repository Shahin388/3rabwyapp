<?php

namespace Modules\Orders\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Categories\Models\Place;
use Modules\Categories\Resources\PlaceResource;
use Carbon\Carbon;

class UserOrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'status'    =>  $this->status,
            'time_ago'  =>  \Carbon\Carbon::parse($this->created_at)->diffForHumans() ,
            'date' => $this->created_at,
            'receipt_location'  =>  $this->receipt_location,
            'delivery_location' => $this->delivery_location,
            'delivery_name'  =>  $this->delivery_name,
            'receipt_name' => $this->receipt_name,
            'delivery_address'  =>  $this->delivery_address,
            'receipt_address' => $this->receipt_address,
            'agent' =>  $this->agent,
            'note'  =>  $this->notes,
            'total'  =>  $this->total ?? 0,
            'image' =>  'http://arbawy.net/3rbawyapp/public/uploads/'.$this->image,
            'user'  =>  $this->user,
        ];
    }
}
