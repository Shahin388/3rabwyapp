<?php

namespace Modules\Pages\Controllers;

use App\Http\Controllers\Controller;
use Modules\Pages\Models\Page;

class ApiController extends Controller
{

    public function index($id = null)
    {
        if ($id) {
            $pages = Page::find($id);
        } else {
            $pages = Page::get(['id', 'title', 'content', 'image']);
        }
        return api_response(1, '', $pages);
    }

    public function about()
    {
        return api_response(1, '', Page::whereType('about')->first());
    }

    public function return_policy()
    {
        return api_response(1, '', Page::whereType('return_policy')->first());
    }

    public function policy()
    {
        return api_response(1, '', Page::whereType('policy')->first());
    }

    public function terms()
    {
        return api_response(1, '', Page::whereType('terms')->first());
    }
}
