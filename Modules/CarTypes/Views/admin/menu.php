<?php

$menu['Content']['CarTypes'] = [
    'title' =>  __('Car types'),
    'link'  =>  '#',
    'icon'  =>  'fas fa-car',
    'childs'    =>  [
        [
            'title' =>  __("Types"),
            'link'  =>  'car_types.index'
        ],
        [
            'title' =>  __("Brands"),
            'link'  =>  'car_brands.index'
        ]
    ]

];
