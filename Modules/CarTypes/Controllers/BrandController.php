<?php

namespace Modules\CarTypes\Controllers;

use Modules\CarTypes\Models\Brand;
use Modules\CarTypes\Models\Type;
use Modules\Common\Controllers\HelperController;

class BrandController extends HelperController
{
    public function __construct()
    {
        $this->model = new Brand;
        $this->title = __('Car Brands');
        $this->name =  'car_brands';
        $this->list = ['name' => 'الاسم', 'type_name' => 'النوع'];

        $types = [];
        $values = Type::get(['name', 'id']);
        foreach ($values as $type) $types[$type->id] = $type->name->ar;
        $this->inputs = [
            'type_id' =>  ['type' => 'select', 'title' => 'المحافظة', 'empty' => 1, 'values' => $types]
        ];
        $this->lang_inputs = [
            'name' => ['title' =>  'الاسم ']
        ];
    }
}
