<?php

namespace Modules\CarTypes\Controllers;

use App\Http\Controllers\Controller;
use Modules\CarTypes\Models\Brand;
use Modules\CarTypes\Models\Type;

class ApiController extends Controller
{
    public function types()
    {
        return api_response(1, '', Type::get());
    }

    public function brands()
    {
        $brands = Brand::when(request('type_id'), function ($query) {
            return $query->where('type_id', request('type_id'));
        })->get();
        return api_response(1, '', $brands);
    }
}
