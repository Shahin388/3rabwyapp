<?php

namespace Modules\CarTypes\Controllers;

use Modules\CarTypes\Models\Type;
use Modules\Common\Controllers\HelperController;

class TypeController extends HelperController
{
    public function __construct()
    {
        $this->model = new Type();
        $this->title = __('Car types');
        $this->name =  'car_types';
        $this->list = ['name' => 'الاسم'];

        $this->lang_inputs = [
            'name' => ['title' =>  'الاسم ']
        ];
    }
}
