<?php

namespace Modules\CarTypes\Models;

use Illuminate\Database\Eloquent\Builder;
use Modules\Common\Models\HelperModel;

class Type extends HelperModel
{
    protected $table = 'car_types';
    protected $fillable = ['name'];

    protected $casts = ['name' => 'array'];

    protected static function booted()
    {
        static::addGlobalScope('istype', function (Builder $builder) {
            $builder->where('type_id', null);
        });
    }
}
