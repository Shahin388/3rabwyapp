<?php

namespace Modules\CarTypes\Models;

use Illuminate\Database\Eloquent\Builder;
use Modules\Common\Models\HelperModel;

class Brand extends HelperModel
{
    protected $table = 'car_types';
    protected $fillable = ['name', 'type_id'];

    protected $casts = ['name' => 'array'];

    protected static function booted()
    {
        static::addGlobalScope('isbrand', function (Builder $builder) {
            $builder->where('type_id', '!=', null);
        });
    }

    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id');
    }

    public function getTypeNameAttribute()
    {
        return $this->type->name ?? '';
    }
}
