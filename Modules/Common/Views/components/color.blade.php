<!-- bootstrap color picker -->
<script src="{{ url('assets/admin') }}/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<div class="form-group">
    <label>{{ $mytitle }}</label>

    <div class="input-group colorpicker">
        <input type="text" {{ $required }} name="{{ $name }}" value="{{ $value }}" class="form-control">

        <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-square"></i></span>
        </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->
<!-- bootstrap color picker -->
<script src="{{ url('assets/admin') }}/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script>
    $('.colorpicker').colorpicker();
    $('.colorpicker').on('colorpickerChange', function(event) {
      $('.colorpicker .fa-square').css('color', event.color.toString());
    });
</script>
