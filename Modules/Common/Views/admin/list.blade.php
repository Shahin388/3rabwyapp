@extends('Common::admin.layout.page')
@section('page')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ __($title) }}</h3>
                <a href="{{ route("admin.$name.create") }}" class="mlink btn btn-success"><i class="fa fa-plus"></i>
                    <span>{{ __("Add new") }}</span></a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example2" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            @foreach($list as $key =>$col_title)
                            <th>{{ app()->getLocale() == 'ar' ? $col_title : ucfirst($key) }}</th>
                            @endforeach
                            @if(isset($links))
                            @foreach($links as $link)
                            <th>{{  __($link['title']) }}</th>
                            @endforeach
                            @endif
                            <th>{{ __("Edit") }}</th>
                            <th>{{ __("Delete") }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rows as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            @foreach($list as $key => $col_title)
                            @php $value = $key != 'created_at' && is_object($row->$key) ? $row->$key->{app()->getLocale()} : $row->$key;
                            @endphp
                            @if(in_array($key , ['image' , 'path']))
                            <td><img src="{{ $value }}" /></td>
                            @else
                            <td>{{ $value }}</td>
                            @endif
                            @endforeach

                            @if(isset($links))
                            @foreach($links as $link)
                            <td>
                                <a class="btn btn-{{ $link['type'] }} mlink"
                                    href="{{ $link['url'].'?'.$link['key'].'='.$row->id }}">
                                    <i class="fa {{ $link['icon'] }}"></i>
                                </a>
                            </td>
                            @endforeach
                            @endif
                            <td>

                                <a class="btn btn-primary mlink" href="{{ route("admin.$name.edit" , $row->id) }}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            <td>
                                <form action="{{ route("admin.$name.destroy" , $row->id) }}" method="post"
                                    class="action_form remove">
                                    @csrf
                                    {{ method_field('delete') }}
                                    <button type="submit" class="btn btn-danger removethis">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $rows->links() }}
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<script>
    // $('.table').DataTable({
    //     "paging": false,
    //     "lengthChange": false,
    //     "searching": false,
    //     "ordering": true,
    //     "info": false,
    //     "autoWidth": false,
    // });

</script>

@stop
