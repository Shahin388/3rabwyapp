<?php
$menu['Common*'] = [
    [
        'title' =>  __("Settings"),
        'link'  =>  'settings.app',
        'icon'  =>  'fas fa-cog'
    ],
    [
        'title' =>  __("App Links"),
        'link'  =>  'settings.app_links',
        'icon'  =>  'fab fa-apple'
    ],
    [
        'title' =>  __('Contacts & Socials'),
        'link'  =>  'settings.contacts',
        'icon'  =>  'fas fa-phone'
    ],
    [
        'title' =>  __("Messages"),
        'link'  =>  'settings.messages',
        'icon'  =>  'fas fa-envelope'
    ],
    // [
    //     'title' =>  __("Home Content"),
    //     'link'  =>  'home_sections.index',
    //     'icon'  =>  'fas fa-pager'
    // ],
    // [
    //     'title' =>  __("Notifications"),
    //     'link'  =>  'notifications',
    //     'icon'  =>  'far fa-bell'
    // ],
    // [
    //     'title' =>  __("Subscriptions"),
    //     'link'  =>  'subscribe.index',
    //     'icon'  =>  'far fa-envelope'
    // ]
];
