<?php

// Route::get('home', 'ApiController@home');

Route::get('notifications', 'ApiController@notifications')->middleware('auth:api');
Route::get('menu', 'ApiController@menu')->middleware('auth:api');
