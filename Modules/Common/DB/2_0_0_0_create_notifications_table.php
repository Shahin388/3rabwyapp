<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->string('model')->default('product');
            $table->string('type', 20)->nullable();
            $table->unsignedBigInteger('user_id')->index()->nullable();
            $table->unsignedBigInteger('offer_id')->index()->nullable();
            $table->unsignedBigInteger('order_id')->index()->nullable();
            $table->text('text');

            $table->text('content');
            $table->boolean('is_read')->default(0);
        
            $table->integer('from_user_id')->unsigned();
            $table->string('notify_type')->nullable();
            $table->boolean('is_active')->default(1)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
