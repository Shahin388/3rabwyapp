<?php

namespace Modules\Common\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Products\Models\Product;
use Modules\User\Models\Rate;
use Modules\User\Models\User;

class HelperModel extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Common relation for all models
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }


    /**
     * Common setter
     * @image
     * @name
     * @brief
     */

    public function getNameAttribute($name)
    {
        if (json_decode($name) && strpos(request()->url(), 'admin') === false) {
            $locale = app()->getLocale();
            return json_decode($name)->$locale;
        }
        return json_decode($name);
    }

    public function getBriefAttribute($brief)
    {
        if (json_decode($brief) && strpos(request()->url(), 'admin') === false) {
            $locale = app()->getLocale();
            return json_decode($brief)->$locale;
        }
        return json_decode($brief);
    }

    public function getTitleAttribute($title)
    {
        if (json_decode($title) && strpos(request()->url(), 'admin') === false) {
            $locale = app()->getLocale();
            return json_decode($title)->$locale;
        }
        return json_decode($title);
    }

    public function getContentAttribute($content)
    {
        if (json_decode($content) && strpos(request()->url(), 'admin') === false) {
            $locale = app()->getLocale();
            return json_decode($content)->$locale;
        }
        return json_decode($content);
    }


    public function setImageAttribute($image)
    {
        $this->attributes['image'] = $image->store("uploads/" . $this->table);
    }

    public function getImageAttribute($image)
    {
        return $image ? url($image) : url('placeholders/' . $this->table . '.png');
    }

    public function setBannerAttribute($banner)
    {
        $this->attributes['banner'] = $banner->store("uploads/" . $this->table);
    }

    public function getBannerAttribute($banner)
    {
        return $banner ? url($banner) : url('placeholders/banner.png');
    }

    public function rates()
    {
        return $this->morphMany(Rate::class, 'rated')->select('id', 'user_id', 'rate');
    }

    public function getRateAttribute()
    {
        // return 0;
        if ($count = $this->rates()->count()) {
            return round(($this->rates()->sum('rate') / (5 * $count)) * 5, 1);
        }
        return 0;
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notified');
    }

    public function getCreatedAtAttribute($created_at)
    {
        return date('Y-m-d', strtotime($created_at));
    }
}
