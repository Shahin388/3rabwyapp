<?php

namespace Modules\Common\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Orders\Models\Guest;
use Modules\Orders\Models\Offer;
use Modules\Orders\Models\Order;
use Modules\Products\Models\Product;
use Modules\User\Models\User;

class Notification extends Model
{
    protected $fillable = ['type', 'model', 'user_id', 'guest_id' , 'offer_id' , 'text', 'guest_id', 'product_id', 'order_id'];
    protected $dates = ['created_at'];
    protected $hidden = ['model', 'updated_at'];
    protected $with = ['user'];

    public function scopeGlobal($query)
    {
        return $query->where('type', 'global');
    }

    public function user()
    {
        return $this->belongsTo(User::class)->select('id', 'username');
    }


    public function guest()
    {
        return $this->belongsTo(Guest::class);
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class , 'offer_id');
    }

    public function setTextAttribute($text)
    {
        if (is_array($text)) {
            $text = json_encode($text);
        }
        $this->attributes['text'] = $text;
    }

    public function getTextAttribute($text)
    {
        if ($decoded_text = json_decode($text)) {
            return $decoded_text->{app()->getLocale()};
        }
        // dd($this->offer->agent->username);
        if ($this->type == 'offer') {
            return __($text, ['user' => $this->offer->agent->username]);
        } elseif ($this->model = 'order') {
            return __($text, ['num' => $this->order_id]);
        }
        return $text;
    }

    public function getModelIdAttribute()
    {
        if ($this->model == 'barber') {
            return $this->barber_id;
        }
        return $this->order_id;
    }

    public function getImageAttribute()
    {
        if ($this->model == 'order') {
            return Order::find($this->order_id)->products[0]->image ?? null;
        }
        return Product::find($this->product_id)->image ?? null;
    }

    public function getCreatedAtAttribute($created_at)
    {
        $created_at = Carbon::parse($created_at);
        return $created_at->diffForHumans();
    }
}
