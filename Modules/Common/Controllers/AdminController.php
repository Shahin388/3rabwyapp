<?php

namespace Modules\Common\Controllers;

use App\Http\Controllers\Controller;
use Modules\Categories\Models\Category;
use Modules\Common\Models\Notification;
use Modules\Common\Models\Setting;
// use Modules\Orders\Models\Guest;
use Modules\Orders\Models\Order;
use Modules\Products\Models\Product;
use Modules\User\Models\Device;
use Modules\User\Models\User;

class AdminController extends Controller
{
    public function home()
    {
        $roles = auth()->user()->role->roles;
        if (!in_array('Common', $roles)) {
            return redirect()->route('admin.' . strtolower($roles[0]) . '.index');
        }
        $title = __("Home page");
        $counters = [
            [
                'title' =>  __("Users"),
                'url'   =>  route('admin.users.index'),
                'count' =>  User::count(),
                'icon'  =>  'fa-users',
                'type'  =>  'success'
            ],
            [
                'title' =>  __("Categories"),
                'url'   =>  route('admin.categories.index'),
                'count' =>  Category::count(),
                'icon'  =>  'fa-th-large',
                'type'  =>  'warning'
            ],
            [
                'title' =>  __("Orders"),
                'url'   =>  route('admin.orders.index'),
                'count' =>  Order::count(),
                'icon'  =>  'fa-shopping-cart',
                'type'  =>  'danger'
            ]
        ];
        $months = range(1, 12);
        $sales = [];
        foreach ($months as $month => $month_name) {
            $month++;
            $sales[] = Order::whereMonth('created_at', $month)->sum('total') ?? 0;
        }
        // dd($sales, $months);
        $orders = [
            'inprogress' =>  (int) Order::whereStatus('inprgoress')->count() ?? 0,
            'done' =>  (int) Order::whereStatus('done')->count() ?? 0,
            'pending' =>  (int) Order::whereStatus('pending')->count() ?? 0,
        ];

        $users = User::count();
        // $guests = Guest::count();

        $devices = [
            Device::where('device_type', 'android')->count(),
            Device::where('device_type', 'ios')->count()
        ];

        // $products = Product::latest()->take(10)->get();
        $latest_orders = Order::latest()->take(10)->get();
        return view('Common::admin.home', get_defined_vars());
    }

    public function load()
    {
        $title = "";
        return view('Common::admin.load', get_defined_vars());
    }

    public function settings()
    {
        if (request()->isMethod('post')) {
            $data = request()->except('_token');
            foreach ($data as $key => $value) {
                Setting::firstOrCreate(['key' => $key, 'type' => 'settings'])->update(['value' => $value]);
            }
            return response()->json(['url' => route('admin.settings.app'), 'message' => 'تم تعديل الإعدادات بنجاح']);
        }
        $lang_inputs = [
            'title' =>  ['title' => 'عنوان التطبيق', 'setting' => 1],
            'logo'  =>  ['title' => 'اللوجو', 'type' => 'image', 'setting' => 1],
            'favicon'  =>  ['title' => 'ايقونة المتصفح', 'type' => 'image', 'setting' => 1],
            'keywords'  =>  ['title' => 'الكلمات الدلالية', 'setting' => 1],
            'description'  =>  ['title' => 'وصف مختصر', 'type' => 'textarea', 'setting' => 1],
        ];
        $action = route('admin.settings.app');
        $method = 'post';
        $title = __("Settings");
        $model = new Setting;
        return view('Common::admin.form', get_defined_vars());
    }


    public function app_links()
    {
        if (request()->isMethod('post')) {
            $data = request()->except('_token');
            foreach ($data as $key => $value) {
                Setting::firstOrCreate(['key' => $key, 'type' => 'settings'])->update(['value' => $value]);
            }
            return response()->json(['url' => route('admin.settings.app_links'), 'message' => 'تم تعديل الإعدادات بنجاح']);
        }
        $lang_inputs = [
            'android' =>  ['title' => 'رابط الاندرويد', 'setting' => 1],
            'ios'  =>  ['title' => 'رابط ال IOS', 'setting' => 1]
        ];
        $action = route('admin.settings.app_links');
        $method = 'post';
        $title = __("App Links");
        $model = new Setting;
        return view('Common::admin.form', get_defined_vars());
    }

    public function messages()
    {
        if (request()->isMethod('post')) {
            $data = request()->except('_token');
            foreach ($data as $key => $value) {
                Setting::firstOrCreate(['key' => $key, 'type' => 'messages'])->update(['value' => $value]);
            }
            return response()->json(['url' => route('admin.settings.messages'), 'message' => __("Info saved successfully")]);
        }
        $groups = [
            __("Email")  =>  [
                'mail_mailer' =>  ['title' => 'نوع المرسل', 'setting' => 1],
                'mail_host' =>  ['title' => 'Host', 'setting' => 1],
                'mail_port' =>  ['title' => 'Port', 'setting' => 1],
                'mail_username' =>  ['title' => 'اسم المستخدم', 'setting' => 1],
                'mail_password' =>  ['title' => 'كلمة السر', 'setting' => 1],
                'mail_encryption' =>  ['title' => 'التشفير', 'setting' => 1],
                'mail_from_address' =>  ['title' => 'بريد المرسل', 'setting' => 1],
                'mail_from_name' =>  ['title' => 'اسم المرسل', 'setting' => 1],
            ],
            __("SMS") =>  [
                'sms_username'  =>  ['title' => 'اسم المستخدم', 'setting' => 1],
                'sms_password'  =>  ['title' => 'كلمة السر', 'setting' => 1],
                'sms_sender'  =>  ['title' => 'اسم المرسل', 'setting' => 1]
            ]
        ];
        $action = route('admin.settings.messages');
        $title = __("Message settings");
        $model = new Setting;
        $method = 'post';
        return view('Common::admin.form', get_defined_vars());
    }

    public function contacts()
    {
        if (request()->isMethod('get')) {
            $action = route('admin.settings.contacts');
            $method = 'post';
            $title = __('Contacts');
            $model = new Setting;
            $contacts = Setting::whereType('contacts')->get();
            // dd($contacts);
            return view('Common::admin.settings.contacts', get_defined_vars());
        }
        $keys = array_filter(request('key'));
        $values = request('value');
        $images = request('image');
        foreach ($keys as $i => $key) {
            $setting = Setting::firstOrCreate(['key' => $key, 'type' => 'contacts']);
            $values && isset($values[$i]) && $values[$i] ? $setting->value = $values[$i] : '';
            $images && isset($images[$i]) && $images[$i] ? $setting->image = $images[$i] : '';
            $setting->type = 'contacts';
            $setting->save();
        }
        return response()->json(['url' => route('admin.settings.contacts'), 'message' => __('Info saved successfully')]);
    }
    public function remove_contact()
    {
        Setting::findOrFail(request('id'))->delete();
        return 'success';
    }

    public function remove_img()
    {
        \DB::table('product_images')->where('id', request('id'))->delete();
        return 'success';
    }


    public function notifications($id = null)
    {
        if (request()->isMethod('get')) {
            if ($id) {
                $row = Notification::findOrFail($id)->toArray();
                unset($row['id'], $row['created_at'], $row['updated_at']);
                Notification::create($row);
                return redirect()->route('admin.notifications');
            }
            $notifications = Notification::global()->latest()->paginate(20);
            $products = Product::get(['id', 'name']);
            $title = "Notifications";
            return view('Common::admin.notifications', get_defined_vars());
        }
        $data = request()->all();
        $data['type'] = 'global';
        Notification::create($data);

        $ar_users = User::where('lang', 'ar')->pluck('id')->toArray();
        $ios_ar_devices = Device::whereIn('user_id', $ar_users)->where('device_type', 'ios')->pluck('device_token')->toArray();
        $ios_ar_devices = implode(',', $ios_ar_devices);
        send_fcm($ios_ar_devices, 'ios', request('text')['ar'], null, request('product_id'));

        $android_ar_devices = Device::whereIn('user_id', $ar_users)->where('device_type', 'android')->pluck('device_token')->toArray();
        $android_ar_devices = implode(',', $android_ar_devices);
        send_fcm($android_ar_devices, 'android', request('text')['ar'], null, request('product_id'));

        $en_users = User::where('lang', 'en')->pluck('id')->toArray();
        $ios_en_devices = Device::whereIn('user_id', $en_users)->where('device_type', 'ios')->pluck('device_token')->toArray();
        $ios_en_devices = implode(',', $ios_en_devices);
        send_fcm($ios_en_devices, 'ios', request('text')['en'], null, request('product_id'));

        $android_en_devices = Device::whereIn('user_id', $en_users)->where('device_type', 'android')->pluck('device_token')->toArray();
        $android_en_devices = implode(',', $android_en_devices);
        send_fcm($android_en_devices, 'android', request('text')['en'], null, request('product_id'));

        return response()->json(['url' => route('admin.notifications'), 'message' => __('Notification sent successfully')]);
    }

    public function notifications_delete($id)
    {
        Notification::find($id)->delete();
        return response()->json(['url' => route('admin.notifications'), 'message' => __('Notification deleted successfully')]);
    }
}
