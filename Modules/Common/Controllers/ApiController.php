<?php

namespace Modules\Common\Controllers;

use App\Http\Controllers\Controller;
use Modules\Ads\Models\Ad;
use Modules\Categories\Models\Category;
use Modules\Common\Models\Notification;
use Modules\Common\Resources\NotificationResource;
use Modules\Orders\Models\Guest;
use Modules\Products\Models\Product;
use Modules\Products\Resources\ProductsResource;

class ApiController extends Controller
{

    public function home()
    {
        $data['ads'] = Ad::latest()->get();
        $data['categories'] = Category::get(['id', 'name', 'image']);
        $products = Product::latest()->paginate(15);
        $data['products'] = ProductsResource::collection($products);
        return api_response(1, '', $data);
    }

    public function notifications()
    {
        $user = auth('api')->user() ?? Guest::where('fb_token', request()->header('FbToken'))->first();
        if (!$user) return api_response('error', 'User or Guest not found');
        $notifications = Notification::where(function ($query) use ($user) {
            return $query->where('user_id', $user->id)->orWhere('guest_id', $user->id);
        })->orWhere('type', 'global')
            ->latest()
            ->paginate(20);

        $notifications = NotificationResource::collection($notifications);
        return api_response(1, '', $notifications);
    }
}
