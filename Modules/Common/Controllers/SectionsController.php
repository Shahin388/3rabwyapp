<?php

namespace Modules\Common\Controllers;

use Modules\Common\Models\Section;

class SectionsController extends HelperController
{

    public function __construct()
    {
        $this->model = new Section();
        $this->title = "Home Content";
        $this->name =  'home_sections';
        $this->list = ['type' => 'النوع', 'title' => 'العنوان'];
        $this->lang_inputs = [
            'title'  =>  ['title' => 'العنوان'],
            'content' => ['title' =>  'الحتوي', 'type'  =>  'editor']
        ];
        $types = [
            'offers'    =>  'العروض',
            'features'  =>  'المميزات'
        ];
        $this->inputs = [
            'type'  =>  ['title'    =>  'النوع', 'type' => 'select', 'values' => $types]
        ];
    }
}
