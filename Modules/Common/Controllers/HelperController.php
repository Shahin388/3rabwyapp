<?php

namespace Modules\Common\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HelperController extends Controller
{
    protected $model;
    protected $name;
    protected $myname;
    protected $list;
    protected $inputs;
    protected $lang_inputs;
    protected $method;
    protected $action;
    protected function index()
    {
        if (auth('stores')->check()) $this->model = $this->model->forStore();
        if (!isset($this->rows)) {
            $this->rows = $this->model;
        }
        $queries = request()->query();
        foreach (request()->query() as $key => $value) {
            if (in_array($key, $this->model->getFillable())) {
                $this->rows = $this->rows->when(request()->has($key), function ($query) use ($key, $value) {
                    return $query->where($key, $value);
                });
            }
        }
        if ($word = request('keyword')) {
            $keys = $this->model->getFillable();
            foreach ($keys as $index => $key) {
                if ($index == 0) {
                    $this->rows = $this->rows->where($key, 'like', '%' . $word . '%');
                } else {
                    $this->rows = $this->rows->orWhere($key, 'like', '%' . $word . '%');
                }
            }
        }
        $this->rows = $this->rows->latest()->paginate(25);
        return view('Common::admin.list', get_object_vars($this));
    }

    protected function create()
    {
        $this->method = 'post';
        $this->action = route("admin." . $this->name . ".store");
        return view('Common::admin.form', get_object_vars($this));
    }

    protected function store(Request $request)
    {
        $data = request()->all();
        if (auth('stores')->check()) {
            $data['store_id'] = auth('stores')->id();
            $this->model = $this->model->forStore();
        }
        $model = $this->model->create($data);
        if ($images = request('images')) {
            foreach ($images as $image) {
                $model->images()->create(['image' => $image]);
            }
        }
        return response()->json(['url' => route('admin.' . $this->name . '.index'), 'message' => __("Info saved successfully")]);
    }

    protected function edit($id)
    {
        if (auth('stores')->check()) $this->model = $this->model->forStore();
        $this->model = $this->model->findOrFail($id);
        $this->method = 'put';
        $this->action = route("admin." . $this->name . ".update", $id);
        return view('Common::admin.form', get_object_vars($this));
    }

    protected function update(Request $request, $id)
    {
        if (auth('stores')->check()) $this->model = $this->model->forStore();
        $this->model = $this->model->findOrFail($id);
        $this->model->update(request()->all());
        if ($images = request('images')) {
            foreach ($images as $image) {
                $this->model->images()->create(['image' => $image]);
            }
        }
        return response()->json(['url' => route('admin.' . $this->name . '.index'), 'message' => __("Info saved successfully")]);
    }

    public function destroy($id)
    {
        if (auth('stores')->check()) $this->model = $this->model->forStore();
        $this->model->findOrFail($id)->delete();
        return response()->json(['url' => route('admin.' . $this->name . '.index'), 'message' => __("Deleted successfully")]);
    }
}
