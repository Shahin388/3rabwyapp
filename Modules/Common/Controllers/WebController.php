<?php

namespace Modules\Common\Controllers;

use App\Http\Controllers\Controller;
use Modules\Common\Models\Section;
use Modules\Common\Models\Setting;

class WebController extends Controller
{

    public function index()
    {
        return redirect()->to("http://arbawy.net/comingsoon/");
        return view('welcome');
        $socials = Setting::socials()->get();
        $rows = Section::get();
        $sections = [];
        foreach ($rows as $section) {
            $sections[$section->type] = $section;
        }
        $android = Setting::where('key' , 'android')->first()->value ?? '#';
        $ios = Setting::where('key' , 'ios')->first()->value ?? '#';
        return view('Common::index', get_defined_vars());
    }
}
