<?php

use Modules\Categories\Models\Place;

function boolean_vals()
{
    return ['No', 'Yes'];
}
function page_types()
{
    return [
        'page' => "Normal page",
        'terms' => "Policy & Rules",
        'policy' => 'Subscription agreement',
        'return_policy' => "Return policy",
        'about' => 'About',
        'contact' => 'Contactus'
    ];
}

function hours()
{
    return range(1, 24);
}

function sidebar()
{
    $menu = [];
    $menu_files = glob(base_path("Modules/**/Views/admin/menu.php"));
    foreach ($menu_files as $file) {
        $content = str_replace("<?php", "", file_get_contents($file));
        eval($content);
    }
    foreach ($menu as $group => $menu) {
        if (strpos($group, '*') !== false) {
            $group = str_replace("*", "", $group);
            $links[$group] = [
                'title' =>  __($group),
                'links' =>  $menu
            ];
        } else {
            $links[] = [
                'title' =>  __($group),
                'links' =>  $menu
            ];
        }
    }

    $roles = auth()->user()->role->roles;
    // dd($roles);
    foreach ($links as $key => $link) {
        if (is_string($key)) {
            if (!in_array($key, $roles)) {
                unset($links[$key]);
            }
        } elseif (isset($link['links'])) {
            $sub_links = $link['links'];
            foreach ($sub_links as $ken => $len) {
                if (!in_array($ken, $roles)) {
                    unset($sub_links[$ken]);
                }
            }
            if (count($sub_links)) {
                $link['links'] = $sub_links;
                $links[$key] = $link;
            } else unset($links[$key]);
        }
    }
    return $links;
}


function admin_roles()
{
    $modules = glob(base_path("Modules/*"));
    foreach ($modules as $module) {
        $module = array_reverse(explode('/', $module))[0];
        if (strpos($module, '.php') === false) {
            $roles[] = $module;
        }
    }
    foreach ($roles  as $role) {
        if (!in_array($role, ['Addresses'])) {
            $rows[$role] = $role;
        }
    }
    return $rows;
}

function order_statuses()
{
    return [
        'inprogress',
        'pending',
        'done',
        'canceled'
    ];
}


function send_fcm($tokens, $platfrom, $message, $order_id, $agent_id = null)
{
    if (is_array($tokens)) {
        $tokens = implode(',', $tokens);
    }
    // dd($tokens, $message, $order_id);
    ob_start();
    $url = "https://fcm.googleapis.com/fcm/send";
    $serverKey = "AAAALueB1UA:APA91bF11Z-x2jx-BbQxTKBnz1-8LhLN-7QRHF7HtLb9cd7Z0wBp_ZK5HXtkW-4F2MyrVL2fPMpFla9q0bN_bMTIZh1OSTm9Zdsudr7JETQgidPZy2LfUdoZyqp_0TxGd92zeu0NR4Pr";

    $type = $order_id ? 'order' : 'agent';
    $id = $order_id ?? $agent_id;
    $notification = array('text' => $message, 'type' => $type, 'id' => $id, 'sound' => 'default', 'badge' => '1','click_action'=>'FLUTTER_NOTIFICATION_CLICK');
    if ($platfrom == 'ios') {
        $arrayToSend = array('to' => $tokens, 'notification' => $notification, 'data' => $notification, 'priority' => 'high');
    } else {
        $arrayToSend = array('to' => $tokens, 'data' => $notification, 'priority' => 'high');
    }
    $json = json_encode($arrayToSend);
    //dd($json);
    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: key=' . $serverKey;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    //Send the request
    $response = curl_exec($ch);
    //dd($response);
    if ($response === FALSE) {
        // die('FCM Send Error: ' . curl_error($ch));
    }
    // dd($json);
    curl_close($ch);
    ob_end_clean();
    return true;
}

function send_sms($numbers, $message)
{
    $key = substr($numbers, 0, 3);
    if (substr($numbers, 0, 3) != '966') {
        $numbers = '966' . $numbers;
    }
    $url = "https://www.hisms.ws/api.php?username=Yorolooop&password=Asd-12345&numbers=$numbers&message" . urlencode($message);
    $result = file_get_contents($url);
    // dd($result);
    return $result;
}

function app_setting($key)
{
    return \Modules\Common\Models\Setting::where('key', $key)->first()->value ?? '';
}


function search_nearby($keyword,$lat,$lang)
{
    //dd($lat);
    $keyword = trim($keyword);
    $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" . urlencode($keyword) . "&language=ar&location=".$lat.",".$lang."&radius=10&key=AIzaSyCrlhK92MhG3urFsESy3u1b5TGAxoFv6RA";
    
    
   
    if ($token = request('next_page_token')) {
        $url .= "&next_page_token=$token";
    }
    $result = file_get_contents($url);
    $places = json_decode($result);
    //dd($places);
    $results = [];
    foreach ($places->results as $place) {
       // dd($place);
        $photo = "";
        if (isset($place->photos)) {
            $photo = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" . $place->photos[0]->photo_reference . "&key=" . env('GOOGLE_KEY');
        }
        $location = $place->geometry->location;
        $info = [
            'id'    =>  $place->place_id,
            'name'  =>  $place->name,
            "photo" =>  $photo,
            'open_now'  =>  $place->opening_hours->open_now ?? false,
            'rating'    =>  $place->rating,
            'address'   =>  $place->formatted_address,
            'location'  =>  $location,
            'distance'  =>  distance($location->lat, $location->lng, request()->header('Lat'), request()->header('Lng'), "K")
        ];
        $place = Place::firstOrCreate(['place_id' => $place->place_id]);
        $place->update(['info' => $info]);
        $info['agents'] = $place->agents()->count();
        $results[] = $info;
    }
    $places->results = $results;
    return $places;
}


function search_nearby2($keyword,$name,$lat,$lang)
{
    //dd($name);
    $keyword = trim($keyword);
     $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" . urlencode($keyword) . "&language=ar&location=".$lat.",".$lang."&radius=10&key=AIzaSyCrlhK92MhG3urFsESy3u1b5TGAxoFv6RA";
    if ($token = request('next_page_token')) {
        $url .= "&next_page_token=$token";
    }
    $result = file_get_contents($url);
    $places = json_decode($result);
    
    $results = [];
    foreach ($places->results as $place) {
        
        $place = $place->where('name'.'like', '%' . $name . '%');
           

        $photo = "";
        if (isset($place->photos)) {
            $photo = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" . $place->photos[0]->photo_reference . "&key=" . env('GOOGLE_KEY');
        }
        $location = $place->geometry->location;
        $info = [
            'id'    =>  $place->id,
            'name'  =>  $place->name,
            "photo" =>  $photo,
            'open_now'  =>  $place->opening_hours->open_now ?? false,
            'rating'    =>  $place->rating,
            'address'   =>  $place->formatted_address,
            'location'  =>  $location,
            'distance'  =>  distance($location->lat, $location->lng, request()->header('Lat'), request()->header('Lng'), "K")
        ];
        $place = Place::firstOrCreate(['place_id' => $place->id]);
        $place->update(['info' => $info]);
        $info['agents'] = $place->agents()->count();
        $results[] = $info;
        
    }
    $places->results = $results;
    return $places;
}


function distance($lat1, $lon1, $lat2, $lon2, $unit)
{

    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
        return number_format(($miles * 1.609344), 2) . ' km';
    } else if ($unit == "N") {
        return ($miles * 0.8684);
    } else {
        return $miles;
    }
}
