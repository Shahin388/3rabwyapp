<?php

namespace Modules\Categories\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceAgent extends Model
{
    protected $table = "place_agents";
    protected $fillable = ['agent_id'];
}
