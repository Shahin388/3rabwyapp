<?php

namespace Modules\Categories\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;

class Place extends Model
{
    protected $fillable = [
        'place_id',
        'info',
        'user_id'
    ];

    protected $casts = ['info' => 'array'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function agents()
    {
        return $this->hasMany(PlaceAgent::class, 'place_id');
    }
}
