<?php

namespace Modules\Categories\Models;

use Modules\Common\Models\HelperModel;

class Category extends HelperModel
{
    protected $fillable = [
        'name'
    ];

    protected $casts = ['name' => 'array'];
}
