<?php

namespace Modules\Categories\Controllers;

use Modules\Categories\Models\Category;
use Modules\Common\Controllers\HelperController;

class AdminController extends HelperController
{
    public function __construct()
    {
        $this->model = new Category();
        $this->title = __('Categories');
        $this->name =  'categories';
        $this->list = ['name' => 'القسم'];

        $this->lang_inputs = [
            'name' => ['title' =>  'اسم القسم']
        ];
    }
}
