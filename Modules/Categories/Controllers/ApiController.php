<?php

namespace Modules\Categories\Controllers;

use App\Http\Controllers\Controller;
use Modules\Categories\Models\Category;
use Modules\Categories\Models\Place;
use Modules\User\Models\User;
use Modules\Categories\Requests\PlaceRequest;
use Modules\Categories\Resources\PlaceResource;
use Modules\Categories\Models\PlaceAgent;
use Auth;
class ApiController extends Controller
{

    public function index()
    {
        $rows = Category::get(['id', 'name']);
        return api_response(1, '', $rows);
    }

    public function store(PlaceRequest $request)
    {
        $data['info'] = $request->all();
        $data['info']['photo'] = '';
        $data['place_id'] = $data['info']['id'] = \Str::random(50);
        $data['info']['distance'] = '';
        $data['info']['open_now'] = true;
        $data['user_id'] = auth('api')->user()->id;
        $place = Place::create($data);
        return api_response(1, '', new PlaceResource($place));
    }

    public function myplaces()
    {
        $places = auth('api')->user()->places()->latest()->get();
        return api_response(1, '', PlaceResource::collection($places));
    }

    public function places()
    {
        
        $keyword = request('category') . ' ' . request('keyword');
        if(request('category_id')){
            $keyword = Category::where('id',request('category_id'))->pluck('name')->first();
            $places = search_nearby($keyword,request('lat'),request('lang'));
        if ($places->status == "REQUEST_DENIED") {
            $places = Place::inRandomOrder()->get();
            $places = PlaceResource::collection($places);
        }
        return api_response(1, '', $places);
        }
        $places = search_nearby($keyword,request('lat'),request('lang'));
        if ($places->status == "REQUEST_DENIED") {
            $places = Place::inRandomOrder()->get();
            $places = PlaceResource::collection($places);
        }
        return api_response(1, '', $places);
    }
    
    public function searchPlace()
    {
        $user = Auth::user();
       // dd($user->lat);
        $keyword = request('category') . ' ' . request('keyword');
        if(request('category_id')){
            $keyword = Category::where('id',request('category_id'))->pluck('name')->first();
            $places = search_nearby($keyword,$user->lat,$user->lang);
        if ($places->status == "REQUEST_DENIED") {
            $places = Place::inRandomOrder()->get();
            $places = PlaceResource::collection($places);
        }
        return api_response(1, '', $places);
        }
        
        if(request('name')){
            // $keyword = Category::where('id',request('category_id'))->pluck('name')->first();
            $name = request('name');
            $places = search_nearby2($keyword,$name,$user->lat,$user->lang);
        if ($places->status == "REQUEST_DENIED") {
            $places = Place::inRandomOrder()->get();
            $places = PlaceResource::collection($places);
        }
        return api_response(1, '', $places);
        }
        $places = search_nearby($keyword,$user->lat,$user->lang);
        if ($places->status == "REQUEST_DENIED") {
            $places = Place::inRandomOrder()->get();
            $places = PlaceResource::collection($places);
        }
        return api_response(1, '', $places);
    }

    public function sign_as_agent()
    {
        $user = auth('api')->user();
        $place = Place::where('place_id', request('place_id'))->firstOrFail();
        $userPlace = PlaceAgent::where('agent_id',$user->id)->where('place_id',$place->id)->pluck('id')->first();
        if(isset($userPlace)){
           $userPlace = PlaceAgent::where('agent_id',$user->id)->where('place_id',$place->id)->first();
           $userPlace->delete();
           $agents = $place->agents()->count();
           return api_response(1, __("You un signed as delegator successfully"), ['agents' => $agents]);
        }else{
        $place->agents()->firstOrCreate(['agent_id' => $user->id]);
        $agents = $place->agents()->count();
        return api_response(1, __("You signed as delegator successfully"), ['agents' => $agents]);
        }
        
    }
}
