<?php

$menu['Services']['Categories'] = [
    'title' =>  __("Categories"),
    'link'  =>  'categories.index',
    'icon'  =>  'fas fa-th-list'
];
