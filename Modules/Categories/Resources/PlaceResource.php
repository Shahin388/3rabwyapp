<?php

namespace Modules\Categories\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlaceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lat = $this->info['location']['lat'] ?? 0;
        $lng = $this->info['location']['lng'] ?? 0;
        $lat = (float) $lat;
        $lng = (float) $lng;
        return [
            'id'    =>  $this->place_id,
            'name'  =>  $this->info['name'] ?? 'Unknow',
            "photo" =>  $this->info['photo'] ?? '',
            'open_now'  =>  true,
            'rating'    =>  $this->rating ?? 4,
            'address'   =>  $this->info['address'] ?? '',
            'location'  =>  ['lat' => $lat, 'lng' => $lng],
            'distance'  =>  distance($lat, $lng, request()->header('Lat'), request()->header('Lng'), "K")
        ];
    }
}
