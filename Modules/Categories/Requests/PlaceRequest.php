<?php

namespace Modules\Categories\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlaceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required',
            'category_id'   =>  'required|exists:categories,id',
            'address'   =>  'required',
            'location'  => 'required|array',
            'location.lat'  =>  'required',
            'location.lng'  =>  'required'
        ];
    }
}
