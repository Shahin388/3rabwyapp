<?php

Route::get('categories', 'ApiController@index');
    Route::post('places', 'ApiController@places');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('searchPlace', 'ApiController@searchPlace');
    Route::get('myplaces', 'ApiController@myplaces');
    Route::post('add_places', 'ApiController@store');
    Route::post('sign_as_agent', 'ApiController@sign_as_agent');
});
