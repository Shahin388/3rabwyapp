<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'API'], function () {

    Route::group(['middleware'=>'auth:api'], function () {
        Route::post('userPlace', 'HomeController@userPlace');
        Route::post('acceptTrip', 'HomeController@acceptTrip');
        Route::post('refuseTrip', 'HomeController@refuseTrip');
        Route::post('finishTrip', 'HomeController@finishTrip');
        Route::get('notifications', 'HomeController@getNotifications');
        Route::get('logout', 'HomeController@logout');

        Route::get('getRooms', 'ChatController@getRooms');
        Route::post('chatRoom', 'ChatController@chatRoom');
        Route::post('sendMessage', 'ChatController@addMessage');
        Route::post('addRate', 'HomeController@addRate');
        Route::post('addTripDetails', 'HomeController@addTripDetails');
        Route::get('currentTrips', 'HomeController@currentTrips');
        Route::get('finishedTrips', 'HomeController@finishedTrips');
    });
});
